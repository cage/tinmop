# a line starting with a '#' is a comment
# The server instance name
# add this entry to your file (the one in your home)
# server                                    = test.server.org

# your username
# add this entry to your file (the one in your home)
# username                                  = username

# theme

use "default-theme.conf"

# some  (many?) terminal  does  not support  invisible  test, if  your
# terminal is not one of those,  set this directive to: "yes" (without
# quotes). If you are a  terminal emulator software maintainer: please
# implements this attribute in your software! :)

# note that bolean value:

# - "false";
# - "null";
# - "nil";
# - "no";
# - "0".

# are  considered  false,  every  other  values  differents  from  the
# aforementioned values  are considered  true (eg: "yes",  "1", "OK"),
# also spaces are not allowed in boolean values.

terminal-support-invisible-attribute = no

# maximum number of attachments allowed for a single toot
# default is 4
#max-numbers-allowed-attachments            = 4

# maximum number of characters allowed for a single toot
# default is 500
#max-message-length                         = 500

# maximum  number  of characters  allowed  for  reporting an  user  to
# instance's admin
#default is 100
#max-report-comment-length                  = 100

# Character to use when replying to a message
reply-quoted-character                      = "> "

# delete the command  history entries that are older  than this number
# of days
purge-history-days-offset                   =  30

# delete the cache entries that are older than this number of days
purge-cache-days-offset                     =  7

# delete the cache entries that are older than this number of days
purge-gemlog-seen-post-days-offset          =  255

# delete the mentions from database that has not been used after this number of days
purge-post-mention-days-offset              = 200

# chosen editor (as shell command line) for compose a message
editor                                      = "nano --locking"

# prefix for comments line when composing a post
# line starting with this prefix will *not* be sent to the server

post.comment-line.prefix                    = "#"

# allowed  languages post,  if the  regex  does not  match the  post's
# language the post is discarded

post.allowed.language                       = ".*"

# default language code por posts sent, the list can be found here:
# https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes

default.post.language                       = "en"

# update gemlog subscriptions when program starts
# (default 'no', change to 'yes' if desired)
start.update.gemlog                         = no

# controls  if mentions  must  be downloaded  (and  maybe deleted  see
# "delete.fetched.mentions") when updating "home" timeline

fetch-mentions-on-updating-home             = yes

# delete mentions fetched from the server

delete.fetched.mentions                     = yes

# directory  to   scan  for   gempub  files,   default  the   same  as
# $XDG_DATA_HOME (usually something like %HOME/.local/share/tinmop/").

# gempub.directory.library                    = /absolute/path/to/your/gempub/library

# close links window when opening the selected link

open-message-link-window.close-after-select = yes

gemini.search-engine.uri        = "gemini://kennedy.gemi.dev/search"

# gemini proxy, forward all the gemini and http requests to the servel
# below, the response  is converted, by the proxy, in  a format fitted
# for a gemini client (e.g HTML is converted to gemtext)

gemini.proxy.uri                = ""

# The width of  the generated table of contents for  gemini pages when
# tinmop is ran as exclusive gemini client (command line option "-G")

gemini.exclusive.mode.toc.width     = 1/5

# The height of the links window for gemini pages when tinmop is ran as
# exclusive gemini client (command line option "-G")

gemini.exclusive.mode.links.height  = 1/4

# the signature file path relative to $HOME

# signature-file                 = ".signature"

# you can filter off users using regexp

# ignore-user-regexp             = "@domain-name$"

# ignore-user-regexp             = "^user-name@domain-name"

# you can  instruct the program  to open some  non gemini link  with a
# program installed with your system like below
# syntax open "REGEXP" with "PROGRAM-NAME" [use cache]
# example:
#
# open "https?://.+mp3" with "mpv"

# you can open mpv in a new terminal this way:
#
# open "https?://.+mp3" with "xterm -e mpv"
# open "mp3$"  with "xterm -e mpv"
# open "m3u8$" with "xterm -e mpv"
# open "ogg$"  with "xterm -e mpv"

# open png files with gimp but cache them before
#                                  ▼▼▼▼▼▼▼▼▼
# open "https?://png$" with "gimp" use cache

# using "no  wait" allow for content  to be opened before  download is
# completed; note  that not all  file types  can be opened  before the
# whole file is available to the opening program
#                                  ▼▼▼▼▼▼▼
# open "mp3$"  with "xterm -e mpv" no wait
#

# using "buffer NUMBER"  after "no wait" allow to  customize the cache
# (in Mib) to be cached before opening the partial downloaded data
#                                          ▼▼▼▼▼▼▼▼▼ buffer 20 Mib
# open "mp4$"  with "xterm -e mpv" no wait buffer 20

# if the  program runs on  TUI, you have  to instruct tinmop  to pause
# itself and wait for external  program to finish running
#                                      ▼▼▼▼▼▼▼▼▼▼▼▼
# open "jpe?g$" with "chafa" use cache use terminal

# to open some kind of files with an editor
#                 ▼▼▼▼▼▼▼▼
# open "txt$" with "editor"

# finally if you  want to open some  kind of file with  tinmop try the
# following: valid values are "tinmop" "me" "internal"
#                                           ▼▼▼▼▼▼▼▼

open "^((gemini://)|(\\.)|(/)).+gmi$"  with "tinmop"
open "^((gemini://)|(\\.)|(/)).+txt$"  with "tinmop"
open "^((gemini://)|(\\.)|(/)).+sh$"   with "tinmop"
open ".gpub$"                          with "tinmop"

# on terminal  tinmop can collect the  images linked to a  gemini page
# and  shows a  collage of  the images,  needs "montage"  program from
# imagemagick (and a graphical environment) to works

# the size of the collage

gemini.images.montage.geometry  = "640x480"

# number of tiles for row and column

gemini.images.montage.tile      = "1"

#########################
# ⚠ EXPERIMENTAL ZONE ⚠ #
#########################

# use fragment of a gemini IRI/URI as a searching criteria for the downloaded page
# E.g. gemini://foo/bar.gmi#baz will jump to the first occurrence of "baz".
# The fragment is used as a, perl compatible, regular expression

experimental.gemini.iri.fragment.regex = no

# Use gemini favicon?
# see  gemini://mozz.us/files/rfc_gemini_favicon.gmi
gemini.fetch.favicon                   = no
