;; tinmop: a multiprotocol client
;; Copyright © cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :fediverse-post-local-search)

(defrule fedisearch-blank (or #\space #\Newline #\Tab)
  (:constant nil))

(defrule fedisearch-blanks (* fedisearch-blank)
  (:constant nil))

(defrule fedisearch-value (+ (not #\Newline))
  (:text t))

(defrule fedisearch-spaces (+ fedisearch-blank)
  (:constant nil))

(defrule fedisearch-column (or
                            "status-id"
                            "account-id"
                            "username"
                            "account"
                            "uri"
                            "content"
                            "rendered-text"
                            "visibility"
                            "sensitive"
                            "spoiler-text"
                            "reblogs-count"
                            "favourites-count"
                            "replies-count"
                            "url"
                            "language"
                            "favourited"
                            "reblogged"
                            "muted"
                            "tags"
                            "application"
                            "redp"
                            "timeline"
                            "folder")
  (:function (lambda (a) (db::quote-symbol a))))

(defrule fedisearch-column-value (and #\" (+ (not #\")) #\")
  (:text t))

(defrule fedisearch-parens-term (and "(" fedisearch-blanks fedisearch-term fedisearch-blanks ")")
  (:function (lambda (a) (strcat "(" (third a) ")"))))

(defrule fedisearch-term (or fedisearch-parens-term
                             fedisearch-and-where
                             fedisearch-or-where
                             fedisearch-like
                             fedisearch-=-term
                             fedisearch-!=-term
                             fedisearch-<-term
                             fedisearch->-term
                             fedisearch-<=-term
                             fedisearch->=-term)
  (:function (lambda (a) (join-with-strings a " "))))

(defrule fedisearch-like (and fedisearch-column fedisearch-spaces "like"
                              fedisearch-spaces fedisearch-column-value)
  (:function (lambda (a) (format nil
                                 "~a like \"%~a%\""
                                 (first a)
                                 (string-trim '(#\") (fifth a))))))

(defrule fedisearch-=-term (and fedisearch-column fedisearch-spaces "="
                                fedisearch-spaces fedisearch-column-value)
  (:function (lambda (a) (format nil
                                 "~a = ~a"
                                 (first a)
                                 (fifth a)))))

(defrule fedisearch-<-term (and fedisearch-column fedisearch-spaces "<" fedisearch-spaces fedisearch-column-value)
  (:function (lambda (a) (format nil
                                 "~a < ~a"
                                 (first a)
                                 (fifth a)))))

(defrule fedisearch->-term (and fedisearch-column fedisearch-spaces ">" fedisearch-spaces fedisearch-column-value)
  (:function (lambda (a) (format nil
                                 "~a > ~a"
                                 (first a)
                                 (fifth a)))))

(defrule fedisearch-<=-term (and fedisearch-column fedisearch-spaces "<=" fedisearch-spaces fedisearch-column-value)
  (:function (lambda (a) (format nil
                                 "~a <= ~a"
                                 (first a)
                                 (fifth a)))))

(defrule fedisearch->=-term (and fedisearch-column fedisearch-spaces ">=" fedisearch-spaces fedisearch-column-value)
  (:function (lambda (a) (format nil
                                 "~a >= ~a"
                                 (first a)
                                 (fifth a)))))

(defrule fedisearch-!=-term (and fedisearch-column fedisearch-spaces "!=" fedisearch-spaces fedisearch-column-value)
  (:function (lambda (a) (format nil
                                 "~a != ~a"
                                 (first a)
                                 (fifth a)))))

(defrule fedisearch-and-where (and fedisearch-term fedisearch-spaces "and" fedisearch-spaces fedisearch-term))

(defrule fedisearch-or-where (and fedisearch-term fedisearch-spaces "or" fedisearch-spaces fedisearch-term))

(defrule fedisearch-where-clause (and "where" fedisearch-spaces (+ fedisearch-term))
  (:function (lambda (a)
               (if (listp a)
                   (strcat "where " (join-with-strings (third a) " "))
                   ""))))

(defrule fedisearch-into (and "into" fedisearch-spaces (+ (not fedisearch-blank)))
  (:function (lambda (a)
               (third a)))
  (:text t))

(defrule fedisearch-query (? (and fedisearch-where-clause (? (and fedisearch-spaces fedisearch-into))))
  (:function (lambda (a)
               (list (first a) (second (second a))))))

(defun parse-search-statuses (query)
  (let* ((parsed             (parse 'fedisearch-query query))
         (where-clause       (first parsed))
         (destination-folder (second parsed))
         (sql-query          (strcat (format nil
                                             "select * from \"~a\" ~@[~a~]"
                                             +view-search-fediverse-statuses+
                                             where-clause))))
    (values (db-utils:query-low-level sql-query)
            destination-folder)))
