;; tinmop: a multiprotocol client
;; Copyright © cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :constants)

(defmacro actual-program-name ()
  `(if (string= +program-name+ "tinmop")
       +program-name+
       ,(format nil "~a (original name: \"tinmop\")" +program-name+)))

(define-constant +help-about-message-no-uri+
    (format nil
            "~a

Copyright © cage

This program is free software:  you can redistribute it and/or modify
it under the terms of the  GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is  distributed in the hope that it  will be useful, but
WITHOUT  ANY   WARRANTY;  without   even  the  implied   warranty  of
MERCHANTABILITY or  FITNESS FOR  A PARTICULAR  PURPOSE.  See  the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program."
            (actual-program-name))
  :test #'string=)

(define-constant +help-about-message+
    (format nil
            "~a~%For bug report please point your browser to:~2%~a~2%Enjoy!"
            +help-about-message-no-uri+ +issue-tracker+)
  :test #'string=)

(define-constant +welcome-message+
  (format nil
 "[ Note: this window can be scrolled using using ⇧ and ⇩ ]
                        _______
                       /_  __(_)___  ____ ___  ____  ____
                        / / / / __ \\/ __ `__ \\/ __ \\/ __ \\
                       / / / / / / / / / / / / /_/ / /_/ /
                      /_/ /_/_/ /_/_/ /_/ /_/\\____/ .___/
                                                 /_/



Welcome to ~a! A gemini, gopher, mastodon/pleroma and kami client.

- for available keychords (sequence of keys to fire a command) help type '?';

- for searching in the help type: 'C-h a' (control and 'h' keys together, then 'a');
  [if this keychords  does not works, ask to your system administrator]

- documentation is available in man format. Type 'man tinmop' at shell's prompt
  or 'C-h m';

- for general information about this program (including license) type 'C-a';

- if you need more help or for bug report: ~a
  (collaborations are welcome too!).

Some useful keycords to start (the man page contains the full list):

· general use
  - 'q' quit program
  - '/' search

· gemini
  - '>' open URL (also kami is supported)
  - 'l' open link window
  - 'b' go back in history of visited URLs
  - '!' search geminispace (contacts gemini://kennedy.gemi.dev/search)

· mastodon/pleroma
  - 'C-t u' update current timeline
  - 'C-t c' change timeline
  - 'v'     view post's attachments
  - 'l'     open a window containing the links in the post

· kamid
  - 'newline' open node
  - 'e'       edit node
  - 'd'       download file
  - 'u'       upload file
  - 'M-u'     mirror local tree on server
  - 'M-d'     mirror remote tree on local computer

Enjoy!

This program is  distributed in the hope that it  will be useful, but
WITHOUT  ANY   WARRANTY;  without   even  the  implied   warranty  of
MERCHANTABILITY or  FITNESS FOR  A PARTICULAR  PURPOSE.  See  the GNU
General Public License for more details."
          (actual-program-name)
          +issue-tracker+)

  :test #'string=)

(define-constant +http-code-ok+               200             :test #'=)

(define-constant +mime-type-jpg+                "image/jpeg"  :test #'string=)

(define-constant +mime-type-png+                "image/png"   :test #'string=)

(define-constant +mime-type-html+               "text/html"   :test #'string=)

(define-constant +mime-type-text+               "text/plain"  :test #'string=)

(define-constant +db-file-extension+            "sqlite3"     :test #'string=
  :documentation "the extension filename of the databases")

(define-constant +default-database-username+    "default"     :test #'string=)

(define-constant +default-database-server-name+ "invalid"     :test #'string=)

(define-constant +invalid-domain-name+          "invalid"     :test #'string=)

(define-constant +json-true+                    "true"        :test #'string=)

(define-constant +json-false+                   "false"       :test #'string=)

(define-constant +fps+                         60             :test #'=
                 :documentation "The redraw frequency in frame per second")

(define-constant +command-window-height+        1             :test #'=)

(define-constant +starting-init-file+           "init.lisp"   :test #'string=)

(define-constant +box-height-diff+              3             :test #'=
                 :documentation "When fitting columns of text in a box
                 remove this rows from total height")

(define-constant +default-command-prompt+       "> "          :test #'string=)

(define-constant +menu-button-ok+               "OK"          :test #'string=)

(define-constant +status-public-visibility+     "public"      :test #'string=)

(define-constant +status-direct-visibility+     "direct"      :test #'string=)

(define-constant +folder-tag-prefix+            "#"           :test #'string=
                 :documentation "The prefix for messages hashtag")

(define-constant +folder-direct-message-prefix+ "@"           :test #'string=
                 :documentation "The prefix for direct message folder, unused")

(define-constant +mention-prefix+               "@"           :test #'string=
                 :documentation "The prefix for a mention in a message")

(define-constant +cache-tls-certificate-type+   "certificate" :test #'string=
                 :documentation "The cache type for TLS certificate")

(define-constant +standard-editor+              "ed"          :test #'string=
                 :documentation "Ed is the standard editor!")

(define-constant +octet-type+  '(unsigned-byte 8)                           :test #'equalp)

(define-constant +gemini-file-extension+           "gmi"                    :test #'string=)

(define-constant +file-scheme+                     "file"                   :test #'string=)

(define-constant +http-scheme+                     "http"                   :test #'string=)

(define-constant +internal-about-scheme+           "about"                  :test #'string=)

(define-constant +internal-scheme-bookmark+        "bookmark"               :test #'string=)

(define-constant +internal-scheme-gemlogs+         "gemlog"                 :test #'string=)

(define-constant +internal-scheme-view-source+     "view-source"            :test #'string=)

(define-constant +internal-scheme-local-posts+     "fediverse-virtual-path" :test #'string=)

(define-constant +fediverse-account-name-server-separator+ "@" :test #'string=)

(define-constant +gemini-search-engine+ "gemini://kennedy.gemi.dev/search" :test #'string=)

(define-constant +path-env-var-separator+ ":" :test  #'string=)

(define-constant +language-codes+ '("ab"
                                    "aar"
                                    "af"
                                    "aka"
                                    "sq"
                                    "am"
                                    "ara"
                                    "an"
                                    "hy"
                                    "as"
                                    "ava"
                                    "ae"
                                    "aym"
                                    "az"
                                    "bm"
                                    "ba"
                                    "eus"
                                    "be"
                                    "bn"
                                    "bi"
                                    "bs"
                                    "bre"
                                    "bg"
                                    "my"
                                    "an"
                                    "ch"
                                    "ce"
                                    "ny"
                                    "zh"
                                    "cu"
                                    "cv"
                                    "kw"
                                    "co"
                                    "cre"
                                    "hr"
                                    "ces"
                                    "dan"
                                    "dv"
                                    "nl"
                                    "dz"
                                    "en"
                                    "eo"
                                    "et"
                                    "ewe"
                                    "fo"
                                    "fij"
                                    "fi"
                                    "fra"
                                    "fy"
                                    "ful"
                                    "gd"
                                    "gl"
                                    "lug"
                                    "ka"
                                    "deu"
                                    "el"
                                    "kl"
                                    "gn"
                                    "gu"
                                    "ht"
                                    "hau"
                                    "heb"
                                    "her"
                                    "hin"
                                    "ho"
                                    "hu"
                                    "is"
                                    "ido"
                                    "ibo"
                                    "id"
                                    "ia"
                                    "ie"
                                    "iu"
                                    "ik"
                                    "gle"
                                    "it"
                                    "ja"
                                    "jv"
                                    "kn"
                                    "kau"
                                    "ks"
                                    "kaz"
                                    "km"
                                    "ki"
                                    "rw"
                                    "ky"
                                    "kom"
                                    "kon"
                                    "kor"
                                    "ma"
                                    "ku"
                                    "lao"
                                    "lat"
                                    "lv"
                                    "li"
                                    "ln"
                                    "lt"
                                    "lu"
                                    "lb"
                                    "mk"
                                    "mg"
                                    "msa"
                                    "ml"
                                    "mt"
                                    "glv"
                                    "mri"
                                    "mr"
                                    "mh"
                                    "mn"
                                    "nau"
                                    "nv"
                                    "nd"
                                    "nr"
                                    "ndo"
                                    "nep"
                                    "no"
                                    "nb"
                                    "nn"
                                    "ii"
                                    "oc"
                                    "oji"
                                    "ori"
                                    "orm"
                                    "os"
                                    "pli"
                                    "ps"
                                    "fa"
                                    "pol"
                                    "pt"
                                    "pa"
                                    "qu"
                                    "ro"
                                    "rm"
                                    "run"
                                    "ru"
                                    "se"
                                    "smo"
                                    "sag"
                                    "sa"
                                    "sc"
                                    "sr"
                                    "sna"
                                    "snd"
                                    "se"
                                    "slk"
                                    "sl"
                                    "som"
                                    "st"
                                    "an"
                                    "su"
                                    "sw"
                                    "ssw"
                                    "sv"
                                    "tl"
                                    "ty"
                                    "tgk"
                                    "tam"
                                    "tat"
                                    "tel"
                                    "tha"
                                    "bo"
                                    "ti"
                                    "to"
                                    "tso"
                                    "tsn"
                                    "tr"
                                    "tk"
                                    "twi"
                                    "ug"
                                    "uk"
                                    "urd"
                                    "uzb"
                                    "ven"
                                    "vi"
                                    "vo"
                                    "wa"
                                    "cym"
                                    "wol"
                                    "xho"
                                    "yi"
                                    "yor"
                                    "za"
                                    "zul")
  :test #'equalp)
