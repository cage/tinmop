;; tinmop: a multiprotocol client
;; Copyright © cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :gemini-parser-tests)

(defsuite gemini-parser-suite (all-suite))

(defparameter *gemini-stream*
  "# header-1
``` preformatted-1-alt
preformatted-line-1-1
preformatted-line-1-2
preformatted-line-1-3
```
# header-2

## header-3

* list-a
* list-b

foo
``` preformatted-2-alt
preformatted-line-2-1
preformatted-line-2-2
preformatted-line-2-3
```

bar
* list -c
baz

> quote line
> quote line-2
")

(defun make-threads-parse (function)
  (let* ((threads (loop repeat 10
                        collect
                        (progn
                          (sleep .01)
                          (make-thread function))))
         (results (loop for thread in threads
                        collect
                        (join-thread thread))))
    (elt results (random (length results)))))

(defun parse-stream ()
  (make-threads-parse (lambda () (gemini-parser:parse-gemini-file *gemini-stream*
                                                                  :initialize-parser t
                                                                  :add-metadata      nil))))

(deftest parse-test (gemini-parser-suite)
  (assert-equality (lambda (a b) (tree-equal a b :test #'string=))
      '((:h1
         ((:source-line "# header-1
"))
         " header-1")
        (:pre
         ((:source-line "``` preformatted-1-alt
")
          (:alt " preformatted-1-alt")))
        (:as-is
         ((:source-line "preformatted-line-1-1
")
          (:alt " preformatted-1-alt"))
         "preformatted-line-1-1")
        (:as-is
         ((:source-line "preformatted-line-1-2
")
          (:alt " preformatted-1-alt"))
         "preformatted-line-1-2")
        (:as-is
         ((:source-line "preformatted-line-1-3
")
          (:alt " preformatted-1-alt"))
         "preformatted-line-1-3")
        (:pre-end
         ((:source-line "```
")
          (:alt " preformatted-1-alt")))
        (:h1
         ((:source-line "# header-2
"))
         " header-2")
        nil
        (:h2
         ((:source-line "## header-3
"))
         " header-3")
        nil
        (:li
         ((:source-line "* list-a
"))
         "list-a")
        (:li
         ((:source-line "* list-b
"))
         "list-b")
        nil
        (:text
         ((:source-line "foo
"))
         "foo")
        (:pre
         ((:source-line "``` preformatted-2-alt
")
          (:alt " preformatted-2-alt")))
        (:as-is
         ((:source-line "preformatted-line-2-1
")
          (:alt " preformatted-2-alt"))
         "preformatted-line-2-1")
        (:as-is
         ((:source-line "preformatted-line-2-2
")
          (:alt " preformatted-2-alt"))
         "preformatted-line-2-2")
        (:as-is
         ((:source-line "preformatted-line-2-3
")
          (:alt " preformatted-2-alt"))
         "preformatted-line-2-3")
        (:pre-end
         ((:source-line "```
")
          (:alt " preformatted-2-alt")))
        nil
        (:text
         ((:source-line "bar
"))
         "bar")
        (:li
         ((:source-line "* list -c
"))
         "list -c")
        (:text
         ((:source-line "baz
"))
         "baz")
        nil
        (:quote
         ((:source-line "> quote line
"))
         " quote line")
        (:quote
         ((:source-line "> quote line-2
"))
         " quote line-2"))
      (parse-stream)))

(defun annotate-stream ()
  (make-threads-parse (lambda ()
                        (gemini-parser:with-initialized-parser
                        (gemini-parser:add-metadata-to-parsed-gemini-lines
                         (gemini-parser:parse-gemini-file *gemini-stream*
                                                          :add-metadata nil))))))

(deftest annotate-test ((gemini-parser-suite) (parse-test))
  (assert-equality (lambda (a b) (tree-equal a b
                                             :test (lambda (a b)
                                                     (if (numberp a)
                                                         (= a b)
                                                         (string= a b)))))
      '((:h1
         ((:source-id 0) (:header-group-id 0)
          (:source-line "# header-1
"))
         " header-1")
        (:pre
         ((:source-id 1) (:pre-group-id 0) (:pre-alt-text "preformatted-1-alt")
          (:header-group-id 0)
          (:source-line "``` preformatted-1-alt
")
          (:alt " preformatted-1-alt")))
        (:as-is
         ((:source-id 2) (:pre-group-id 0) (:pre-alt-text "preformatted-1-alt")
          (:header-group-id 0)
          (:source-line "preformatted-line-1-1
")
          (:alt " preformatted-1-alt"))
         "preformatted-line-1-1")
        (:as-is
         ((:source-id 3) (:pre-group-id 0) (:pre-alt-text "preformatted-1-alt")
          (:header-group-id 0)
          (:source-line "preformatted-line-1-2
")
          (:alt " preformatted-1-alt"))
         "preformatted-line-1-2")
        (:as-is
         ((:source-id 4) (:pre-group-id 0) (:pre-alt-text "preformatted-1-alt")
          (:header-group-id 0)
          (:source-line "preformatted-line-1-3
")
          (:alt " preformatted-1-alt"))
         "preformatted-line-1-3")
        (:pre-end
         ((:source-id 5)
          (:source-line "```
")
          (:alt " preformatted-1-alt")))
        (:h1
         ((:source-id 6) (:header-group-id 1)
          (:source-line "# header-2
"))
         " header-2")
        (:vertical-space ((:source-id 7)))
        (:h2
         ((:source-id 8) (:header-group-id 2)
          (:source-line "## header-3
"))
         " header-3")
        (:vertical-space ((:source-id 9)))
        (:li
         ((:source-id 10) (:list-id 0) (:header-group-id 2)
          (:source-line "* list-a
"))
         "list-a")
        (:li
         ((:source-id 11) (:list-id 0) (:header-group-id 2)
          (:source-line "* list-b
"))
         "list-b")
        (:vertical-space ((:source-id 12)))
        (:text
         ((:source-id 13) (:header-group-id 2)
          (:source-line "foo
"))
         "foo")
        (:pre
         ((:source-id 14) (:pre-group-id 1) (:pre-alt-text "preformatted-2-alt")
          (:header-group-id 2)
          (:source-line "``` preformatted-2-alt
")
          (:alt " preformatted-2-alt")))
        (:as-is
         ((:source-id 15) (:pre-group-id 1) (:pre-alt-text "preformatted-2-alt")
          (:header-group-id 2)
          (:source-line "preformatted-line-2-1
")
          (:alt " preformatted-2-alt"))
         "preformatted-line-2-1")
        (:as-is
         ((:source-id 16) (:pre-group-id 1) (:pre-alt-text "preformatted-2-alt")
          (:header-group-id 2)
          (:source-line "preformatted-line-2-2
")
          (:alt " preformatted-2-alt"))
         "preformatted-line-2-2")
        (:as-is
         ((:source-id 17) (:pre-group-id 1) (:pre-alt-text "preformatted-2-alt")
          (:header-group-id 2)
          (:source-line "preformatted-line-2-3
")
          (:alt " preformatted-2-alt"))
         "preformatted-line-2-3")
        (:pre-end
         ((:source-id 18)
          (:source-line "```
")
          (:alt " preformatted-2-alt")))
        (:vertical-space ((:source-id 19)))
        (:text
         ((:source-id 20) (:header-group-id 2)
          (:source-line "bar
"))
         "bar")
        (:li
         ((:source-id 21) (:list-id 1) (:header-group-id 2)
          (:source-line "* list -c
"))
         "list -c")
        (:text
         ((:source-id 22) (:header-group-id 2)
          (:source-line "baz
"))
         "baz")
        (:vertical-space ((:source-id 23)))
        (:quote
         ((:source-id 24) (:header-group-id 2)
          (:source-line "> quote line
"))
         " quote line")
        (:quote
         ((:source-id 25) (:header-group-id 2)
          (:source-line "> quote line-2
"))
         " quote line-2"))
      (annotate-stream)))
