;; tinmop: a multiprotocol client
;; Copyright © cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :misc-tests)

(defsuite misc-suite (all-suite))

(deftest test-shuffle (misc-suite)
  (assert-true
      (let ((bag (loop repeat 1000 collect (num:lcg-next-upto 1000))))
        (null (set-difference bag (shuffle bag))))))

(defun read-delimited-excess (&optional (data '(1 2 3 4 5 6 0 7 8)))
  (flexi-streams:with-input-from-sequence (stream data)
    (read-delimited-into-array stream :buffer-size 3)))

(defun read-delimited-aligned-to-buffer (&optional (data '(1 2 3 4 5 0 7 8)))
  (flexi-streams:with-input-from-sequence (stream data)
    (read-delimited-into-array stream :buffer-size 3)))

(defun read-delimited-no-excess (&optional (data '(1 2 3 4 5 6 0)))
  (flexi-streams:with-input-from-sequence (stream data)
    (read-delimited-into-array stream :buffer-size 3)))

(defun read-delimited-error (&optional (data '(1 2 3 4 5 6 7)))
  (flexi-streams:with-input-from-sequence (stream data)
    (read-delimited-into-array stream :buffer-size 3)))

(deftest test-read-delimited-excess (misc-suite)
  (assert-equalp (values #(1 2 3 4 5 6)
                         #(7 8))
      (read-delimited-excess)))

(deftest test-read-delimited-aligned-to-buffer (misc-suite)
  (assert-equalp (values #(1 2 3 4 5)
                         #())
      (read-delimited-aligned-to-buffer)))

(deftest test-read-delimited-no-excess (misc-suite)
  (assert-equalp (values #(1 2 3 4 5 6)
                         #())
      (read-delimited-no-excess)))

(deftest test-read-delimited-no-excess (misc-suite)
  (assert-condition delimiter-not-found
      (read-delimited-error)))

(defun read-delimited-excess-custom-delimiter (&optional (data `(1 2 3
                                                                   4 5 6
                                                                   ,(char-code #\.)
                                                                   7 8 9 10)))
  (flexi-streams:with-input-from-sequence (stream data)
    (read-delimited-into-array stream :buffer-size 4096 :delimiter (char-code #\.))))

(deftest test-read-delimited-custom-delimiter (misc-suite)
  (assert-equalp (values #(1 2 3 4 5 6)
                         #(7 8))
      (read-delimited-excess)))

(defun read-delimited-accum-contains-delimiter ()
  (flexi-streams:with-input-from-sequence (stream #())
    (read-delimited-into-array stream
                               :buffer-size 4096
                               :delimiter 0
                               :accum #(1 2 3 0 4 5 6))))

(deftest test-read-delimited-custom-delimiter (misc-suite)
  (assert-equalp (values #(1 2 3)
                         #(4 5 6))
      (read-delimited-accum-contains-delimiter)))

(defun read-delimited-unbuffered ()
  (flexi-streams:with-input-from-sequence (stream #(1 2 3 0 4 5))
    (read-delimited-into-array-unbuffered stream
                                          :delimiter 0)))

(deftest test-read-delimited-unbuffered (misc-suite)
  (assert-equalp #(1 2 3)
      (read-delimited-unbuffered)))
