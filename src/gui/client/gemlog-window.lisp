;; tinmop: a multiprotocol client
;; Copyright © cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :client-gemlog-window)

(named-readtables:in-readtable nodgui.syntax:nodgui-syntax)

(defclass gemlog-frame (gui-goodies:table-frame) ())

(defun build-column-values (url
                            unseen-count
                            seen-count
                            title
                            subtitle)
  (list url
        unseen-count
        seen-count
        title
        subtitle))

(defmacro build-column-value-accessor (name index)
  (assert (>= index 0))
  (assert (symbolp name))
  `(defun ,(format-fn-symbol t "column-~a" name) (fields)
     (elt fields ,index)))

(build-column-value-accessor url          0)

(build-column-value-accessor unseen-count 1)

(build-column-value-accessor seen-count   2)

(build-column-value-accessor title        3)

(build-column-value-accessor subtitle     4)

(defun resync-rows (gemlog-frame new-rows)
  (with-accessors ((tree gui-goodies:tree)
                   (rows gui-goodies:rows)) gemlog-frame
    (gui:treeview-delete-all tree)
    (setf rows new-rows)
    (loop for row in rows
          for index-count from 0 do
            (let* ((index (cond
                            ((< index-count 9)
                             (format nil "~a" index-count))
                            ((< 10 index-count 116)
                             (string (code-char (+ 85 index-count))))
                            (t
                             (format nil "~a" index-count))))
                   (url           (db:row-url row))
                   (seen-count    (to-s (db:row-seen-count row)))
                   (unseen-count  (to-s (db:row-unseen-count row)))
                   (title         (db:row-title row))
                   (subtitle      (db:row-subtitle row))
                   (tree-row      (make-instance 'gui:tree-item
                                                 :id             index
                                                 :text           index
                                                 :column-values  (build-column-values url
                                                                                      unseen-count
                                                                                      seen-count
                                                                                      title
                                                                                      subtitle)
                                                 :index          gui:+treeview-last-index+)))
              (gui:treeview-insert-item tree :item tree-row)))
    (gui:treeview-refit-columns-width (gui-goodies:tree gemlog-frame))
    gemlog-frame))

(defun all-rows ()
  (cev:enqueue-request-and-wait-results :gemini-gemlog-all-subscription
                                        1
                                        ev:+standard-event-priority+))

(defmethod initialize-instance :after ((object gemlog-frame) &key &allow-other-keys)
  (with-accessors ((tree gui-goodies:tree)
                   (rows gui-goodies:rows)) object
    (let ((new-rows (all-rows))
          (treeview (make-instance 'gui:scrolled-treeview
                                    :master  object
                                    :pack    '(:side :top :expand t :fill :both)
                                    :columns (list (_ "Address")
                                                   (_ "Unread")
                                                   (_ "Read")
                                                   (_ "Title")
                                                   (_ "Subtitle")))))
      (setf tree treeview)
      (gui:treeview-heading tree  gui:+treeview-first-column-id+
                            :text (_ "Index"))
      (resync-rows object new-rows)
      object)))

(defun url-column-value (row)
  (column-url (gui:column-values row)))

(defun unsubscribe-gemlog-clsr (gemlog-frame)
  (lambda ()
    (a:when-let* ((selections (gui:treeview-get-selection (gui-goodies:tree gemlog-frame))))
      (loop for selection in selections do
        (let ((url (url-column-value selection)))
          (ev:with-enqueued-process-and-unblock ()
            (comm:make-request :gemini-gemlog-unsubscribe 1 url))
          (let ((new-rows (all-rows)))
            (resync-rows gemlog-frame new-rows)))))))

(defun refresh-gemlogs-clsr (window gemlog-frame)
  (lambda ()
    (when (gui:children (gui-goodies:tree gemlog-frame) gui:+treeview-root+)
      (ev:with-enqueued-process-and-unblock ()
        (gui-goodies::with-notify-errors
          (gui-goodies:with-busy* (window)
            (comm:make-request :gemini-gemlog-refresh-all-subscriptions 1)
            (client-main-window::print-info-message (_ "All gemlog refreshed")))))
      (let ((new-rows (all-rows)))
        (resync-rows gemlog-frame new-rows)))))

(defun subscribe-gemlog-or-notify-error (iri)
  (handler-case
      (progn
        (comm:make-request :gemini-gemlog-subscribe 1 iri)
        t)
    (error (e)
      (gui-goodies:notify-request-error e)
      nil)))

(defun subscribe-gemlog-clsr (gemlog-frame)
  (lambda ()
    (let* ((iri             (trim-blanks (gui-mw:text-input-dialog gemlog-frame
                                                                   (_ "Info request")
                                                                   (_ "Please, type the gemlog's address")
                                                                   :text "gemini://")))
           (subscription-ok (subscribe-gemlog-or-notify-error iri)))
      (if subscription-ok
          (let ((new-rows (all-rows)))
            (resync-rows gemlog-frame new-rows))
          (gui-goodies:notify-request-error (format nil
                                                    (_ "Unable to subscribe to ~s")
                                                    iri))))))

(defun open-gemlog-clsr (main-window treeview-gemlogs)
  (lambda (e)
    (declare (ignore e))
    (a:when-let* ((selections (gui:treeview-get-selection (gui-goodies:tree treeview-gemlogs)))
                  (selection  (first selections)))
      (let* ((url      (url-column-value selection))
             (fields   (gui:column-values selection))
             (title    (column-title fields))
             (subtitle (column-subtitle fields)))
        (ev:with-enqueued-process-and-unblock ()
          (let ((parsed-gemlog-entries (comm:make-request :gemini-gemlog-entries-page
                                                          1
                                                          url
                                                          title
                                                          subtitle)))
            (client-main-window:clear-gemtext main-window)
            (client-main-window::initialize-ir-lines main-window)
            (client-main-window::collect-ir-lines url main-window parsed-gemlog-entries)
            (client-main-window:set-title-and-address-bar-text main-window url)
            (client-main-window::set-gemlog-toolbar-button-appearance main-window url)))))))

(defun all-gemlog-posts (gemlog-url)
  (cev:enqueue-request-and-wait-results :gemini-gemlog-entries
                                        1
                                        ev:+standard-event-priority+
                                        gemlog-url))

(defun contextual-menu-clsr (gemlog-frame treeview-widget)
  (labels ((copy-uri ()
             (a:when-let* ((selections (gui:treeview-get-selection treeview-widget))
                           (links      (with-output-to-string (stream)
                                         (if (= (length selections) 1)
                                             (format stream
                                                     "~a"
                                                     (url-column-value (first selections)))
                                             (format stream
                                                     "~{~a~^~%~}"
                                                     (mapcar #'url-column-value selections))))))
               (os-utils:copy-to-clipboard links)
               (client-main-window:print-info-message (n_ "Link copied"
                                                          "Links copied"
                                                          (length selections)))))
           (mark-all-read ()
             (a:when-let* ((selections (gui:treeview-get-selection treeview-widget)))
               (loop for selection in selections do
                 (let ((url (url-column-value selection)))
                   (mapcar (lambda (post)
                             (ev:with-enqueued-process-and-unblock ()
                               (let ((post-url (db:row-post-link post)))
                                 (gui-goodies:with-busy* (gemlog-frame)
                                   (comm:make-request :gemini-mark-gemlog-post-read
                                                      1
                                                      post-url)))))
                           (all-gemlog-posts url))))
               (ev:with-enqueued-process-and-unblock ()
                 (gui-goodies:with-busy* (gemlog-frame)
                   (let ((new-rows (all-rows)))
                     (resync-rows gemlog-frame new-rows)))))))
    (lambda (z)
      (declare (ignore z))
      (let* ((popup-menu (gui:make-menu nil (_"gemlog menu")))
             (x          (gui:screen-mouse-x))
             (y          (gui:screen-mouse-y)))
        (gui:make-menubutton popup-menu (_ "Mark all posts of selected gemlogs as read") #'mark-all-read)
        (gui:make-menubutton popup-menu
                             (_ "Copy selected gemlog links to the clipboard")
                             #'copy-uri)
        (gui:popup popup-menu x y)))))

(defun init-window (master main-window)
  (client-main-window:hide-autocomplete-candidates main-window)
  (gui:with-toplevel (toplevel :master master :title (_ "Gemlogs"))
    (let* ((table              (make-instance 'gemlog-frame :master toplevel))
           (buttons-frame      (make-instance 'gui:frame :master toplevel))
           (subscribe-button   (make-instance 'gui:button
                                              :master  buttons-frame
                                              :image   icons:*document-add*
                                              :command (subscribe-gemlog-clsr table)))
           (unsubscribe-button (make-instance 'gui:button
                                              :master  buttons-frame
                                              :image   icons:*document-delete*
                                              :command (unsubscribe-gemlog-clsr table)))
           (refresh-button     (make-instance 'gui:button
                                              :master  buttons-frame
                                              :image   icons:*refresh*
                                              :command (refresh-gemlogs-clsr toplevel
                                                                             table))))
      (gui-goodies:attach-tooltips (unsubscribe-button (_ "unsubscribe from selected gemlog"))
                                   (refresh-button     (_ "refresh all subscription")))
      (gui:grid table              0 0 :sticky :nwe)
      (gui:grid buttons-frame      1 0 :sticky :s)
      (gui:grid subscribe-button   0 0 :sticky :s)
      (gui:grid unsubscribe-button 0 1 :sticky :s)
      (gui:grid refresh-button     0 2 :sticky :s)
      (gui:bind (gui:treeview (gui-goodies:tree table))
                #$<<TreeviewSelect>>$
                (open-gemlog-clsr main-window table))
      (gui:bind (gui:treeview (gui-goodies:tree table))
                #$<3>$
                (contextual-menu-clsr table (gui:treeview (gui-goodies:tree table))))
      (gui:bind (gui:treeview (gui-goodies:tree table))
                #$<KeyPress>$
                (lambda (e)
                  (flet ((clamp-index (items index)
                           (let ((actual-index (a:clamp index
                                                        0
                                                        (1- (length items)))))
                             (list (elt items
                                        (- (1- (length items))
                                           actual-index))))))
                    (let* ((treeview          (gui:treeview (gui-goodies:tree table)))
                           (items             (gui:items treeview))
                           (pressed-char      (gui:event-char e))
                           (pressed-char-code (gui:event-char-code e)))
                      (when (cl-ppcre:scan "(?i)^[a-z0-9]$" pressed-char)
                        (if (ignore-errors (parse-integer pressed-char))
                            (let ((index (clamp-index items (parse-integer pressed-char))))
                              (gui:treeview-set-selection treeview index))
                            (let ((index (clamp-index items (+ 10 (- pressed-char-code
                                                                     97)))))
                              (gui:treeview-set-selection treeview index)))))))
                :append t)
      (gui:focus (gui:treeview (gui-goodies:tree table)))
      (gui:transient toplevel master))))
