;; tinmop: a multiprotocol client
;; Copyright © cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :client-main-window)

(defun make-internal-iri (path &optional (query nil) (fragment nil))
  (iri:make-iri +internal-about-scheme+ nil nil nil path query fragment))

(defun internal-iri-bookmark ()
  (make-internal-iri +internal-scheme-bookmark+))

(defun internal-iri-gemlogs ()
  (make-internal-iri +internal-scheme-gemlogs+))

(defun internal-iri-view-source (path &optional (query nil) (fragment nil))
  (iri:make-iri +internal-scheme-view-source+ nil nil nil path query fragment))

(defun show-bookmarks-page (main-window)
  (ev:with-enqueued-process-and-unblock ()
    (let ((parsed-page (comm:make-request :gemini-generate-bookmark-page 1))
          (iri         (internal-iri-bookmark)))
      (set-title-and-address-bar-text main-window (to-s iri))
      (clear-gemtext main-window)
      (client-main-window::initialize-ir-lines main-window)
      (gui:focus (toc-frame main-window))
      (collect-ir-lines (to-s iri) main-window parsed-page))))
