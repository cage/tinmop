;; tinmop: a multiprotocol client
;; Copyright © cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :client-os-utils)

(defun open-resource-with-external-program (main-window resource &key (open-for-edit nil))
  (gui-goodies:with-notify-errors
    (flet ((edit (file)
             (os-utils:open-with-editor file)))
      (alexandria::when-let* ((parsed-as-iri      (iri:iri-parse resource :null-on-error t))
                              (parsed-no-fragment (iri:remove-fragment parsed-as-iri)))
        (let ((program  (if (iri:absolute-url-p resource)
                            (swconf:link-regex->program-to-use (text-utils:to-s parsed-no-fragment))
                            (swconf:link-regex->program-to-use resource))))
          (if program
              (cond
                ((not (os-utils:executable-exists-p program))
                 ;; this       branch      is       a      workaround.
                 ;; os-utils:open-link-with-program  should signal  an
                 ;; error when  a program is  not found but,  for some
                 ;; reason, it will crash  the software, instead; even
                 ;; wrapping           the           call           to
                 ;; 'os-utils:open-link-with-program'             with
                 ;; 'ignore-errors', does not works this workaround is
                 ;; temporary as  new version of SBCL  works fine with
                 ;; this code
                 (error (_ "external program ~a not found, check your configuration file.")
                        program))
                ((swconf:use-editor-as-external-program-p program)
                 (edit resource))
                ((swconf:use-tinmop-as-external-program-p program)
                 (if open-for-edit
                     (edit resource)
                     (client-main-window:open-local-path resource
                                                         main-window
                                                         :force-rendering t)))
                (t
                 (os-utils:open-link-with-program program resource :wait open-for-edit)))
              (if open-for-edit
                  (error (_ "No program defined in configuration file to edit this kind of files"))
                  (os-utils:xdg-open resource))))))))
