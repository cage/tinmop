;; tinmop: a multiprotocol client
;; Copyright © cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :client-events)

(defparameter *stop-events-loop* t)

(defparameter *events-loop-lock* (make-lock "events-loop-lock"))

(defparameter *events-loop-thread* nil)

(defun events-loop-running-p ()
  (misc:with-lock-held (*events-loop-lock*)
    (not *stop-events-loop*)))

(defun stop-events-loop ()
  (misc:with-lock-held (*events-loop-lock*)
    (setf *stop-events-loop* t))
  #+debug-mode (misc:dbg "Stopping gui event thread")
  (ev:with-enqueued-process-and-unblock ()
    #+debug-mode (misc:dbg "Stopping dummy event"))
  #+debug-mode (misc:dbg "Stopped gui event thread"))

(defun start-events-loop ()
  (misc:with-lock-held (*events-loop-lock*)
    (setf *stop-events-loop* nil))
  (setf *events-loop-thread*
        (make-thread (lambda ()
                          (let ((gui:*wish* gui-goodies:*gui-server*))
                            (loop while (events-loop-running-p) do
                              (ev:dispatch-program-events-or-wait))))
                     :name "GUI events loop")))

(defmacro with-enqueue-request ((method-name id &rest args) &body on-error)
  `(ev:with-enqueued-process-and-unblock ()
     (handler-case
         (comm:make-request ,method-name ,id ,@args)
       ,@on-error)))

(defun enqueue-request-and-wait-results (method-name id priority &rest args)
  (if program-events::*already-enqueued*
      (apply #'comm:make-request method-name id args)
      (ev:push-function-and-wait-results (lambda ()
                                           (apply #'comm:make-request method-name id args))
                                         :push-event-fn #'ev:push-event-unblock
                                         :priority priority)))
