;; tinmop: a multiprotocol client
;; Copyright © cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :constants)

(define-constant +minimum-padding+       2 :test #'=)

(define-constant +ps-file-dialog-filter+ '(("PostScript Files" "*.ps")) :test #'equalp)

(define-constant +stream-status-streaming+   :streaming   :test #'eq)

(define-constant +stream-status-canceled+    :canceled    :test #'eq)

(define-constant +stream-status-downloading+ :downloading :test #'eq)

(define-constant +stream-status-completed+   :completed :test #'eq)
