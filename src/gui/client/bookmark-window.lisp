;; tinmop: a multiprotocol client
;; Copyright © cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :client-bookmark-window)

(named-readtables:in-readtable nodgui.syntax:nodgui-syntax)

(defun add-to-bookmark-clsr (toplevel iri-entry section-entry description-entry)
  (lambda ()
    (let* ((iri         (gui:text iri-entry))
           (section     (if (text-utils:string-empty-p (gui:text section-entry))
                            (ui:localized-default-string)
                            (gui:text section-entry)))
           (description (if (text-utils:string-empty-p (gui:text description-entry))
                            iri
                            (gui:text description-entry))))
      (if (gemini-parser:gemini-iri-p iri)
          (ev:with-enqueued-process-and-unblock ()
            (comm:make-request :gemini-bookmark-delete 1 iri)
            (comm:make-request :gemini-bookmark-add 1 iri section description)
            (gui-goodies:info-dialog toplevel
                                     (format nil (_ "The address ~a has been bookmarked") iri))
            (gui:exit-from-toplevel toplevel))
          (gui-goodies:error-dialog toplevel
                                    (format nil (_ "~s is not a valid gemini address") iri))))))

(defun make-edit-window (master iri
                                &optional
                                  (section-text     (ui:localized-default-string))
                                  (description-text nil))
  (gui:with-toplevel (toplevel :master master :title (_ "Streams"))
    (gui:transient toplevel master)
    (let* ((entries-width     (gui-goodies:quite-good-dialog-width))
           (iri-label         (make-instance 'gui:label
                                             :width entries-width
                                             :master toplevel
                                             :text (_ "Address")))
           (section-label     (make-instance 'gui:label
                                             :width entries-width
                                             :master toplevel
                                             :text (_ "Section")))
           (description-label (make-instance 'gui:label
                                             :width entries-width
                                             :master toplevel
                                             :text (_ "Description")))
           (iri-entry         (make-instance 'gui:entry
                                             :width entries-width
                                             :master toplevel
                                             :text iri))
           (section-entry     (make-instance 'gui:entry
                                             :width entries-width
                                             :master toplevel
                                             :text section-text))
           (description-entry (make-instance 'gui:entry
                                             :width entries-width
                                             :master toplevel
                                             :text description-text))
           (buttons-frame     (make-instance 'gui:frame :master toplevel))
           (add-button        (make-instance 'gui:button
                                             :master  buttons-frame
                                             :image   icons:*document-add*
                                             :command (add-to-bookmark-clsr toplevel
                                                                            iri-entry
                                                                            section-entry
                                                                            description-entry))))
      (gui-goodies:attach-tooltips (add-button (_ "add address bookmarks page")))
      (gui:grid iri-label         0 0 :sticky :nwes :padx +minimum-padding+)
      (gui:grid iri-entry         1 0 :sticky :nwes :padx +minimum-padding+)
      (gui:grid section-label     2 0 :sticky :nwes :padx +minimum-padding+)
      (gui:grid section-entry     3 0 :sticky :nwes :padx +minimum-padding+)
      (gui:grid description-label 4 0 :sticky :nwes :padx +minimum-padding+)
      (gui:grid description-entry 5 0 :sticky :nwes :padx +minimum-padding+)
      (gui:grid buttons-frame     6 0 :sticky :s    :padx +minimum-padding+)
      (gui:grid add-button        0 0 :sticky :s))))

(defun init-window (master main-window iri)
  (client-main-window:hide-autocomplete-candidates main-window)
  (make-edit-window master iri))

(defun bookmarks-table->lisbox-items (bookmarks-table)
  (loop for bookmark in bookmarks-table
        collect
        (strcat (getf bookmark :value) " " (getf bookmark :description))))

(defun delete-bookmark-clsr (toplevel searchbox bookmarks)
  (lambda ()
    (a:when-let ((selected-indices (gui:listbox-get-selection-index searchbox)))
      (ev:with-enqueued-process-and-unblock ()
        (loop for selected-index in selected-indices do
          (let ((iri (getf (elt bookmarks selected-index) :value)))
            (comm:make-request :gemini-bookmark-delete 1 iri)))
        (gui:listbox-delete searchbox)
        (let* ((bookmarks-table (comm:make-request :gemini-bookmark-table 1))
               (items           (bookmarks-table->lisbox-items bookmarks-table)))
          (gui:listbox-append searchbox items))
        (gui-goodies:info-dialog toplevel
                                 (format nil
                                         (n_ "~a element deleted"
                                             "~a elements deleted"
                                             (length selected-indices))
                                         (length selected-indices)))))))

(defun update-bookmark-clsr (toplevel searchbox bookmarks)
  (lambda ()
    (a:when-let ((selected-index (first (gui:listbox-get-selection-index searchbox))))
      (ev:with-enqueued-process-and-unblock ()
        (let ((iri (getf (elt bookmarks selected-index) :value)))
          (comm:make-request :gemini-bookmark-delete 1 iri)
          (make-edit-window toplevel
                            iri
                            (getf (elt bookmarks selected-index) :section)
                            (getf (elt bookmarks selected-index) :description)))))))

(defun manage-bookmarks (master)
  (gui:with-toplevel (toplevel :master master :title (_ "Bookmarks"))
    (gui:transient toplevel master)
    (let* ((bookmarks (cev:enqueue-request-and-wait-results :gemini-bookmark-table
                                                            1
                                                            ev:+standard-event-priority+))
           (items     (bookmarks-table->lisbox-items bookmarks))
           (searchbox (make-instance 'gui-mw:searchable-listbox
                                     :export-selection nil
                                     :select-mode      :extended
                                     :data             items
                                     :entry-labe       (_ "Filter:")
                                     :master           toplevel
                                     :remove-non-matching-p t
                                     :matching-fn (lambda (a b)
                                                    (re:scan (strcat "(?i)" a) b))))
           (buttons-frame (make-instance 'gui:frame :master toplevel))
           (delete-button (make-instance 'gui:button
                                         :master  buttons-frame
                                         :image   icons:*document-delete*
                                         :command (delete-bookmark-clsr toplevel
                                                                        searchbox
                                                                        bookmarks)))
           (edit-button  (make-instance 'gui:button
                                         :master  buttons-frame
                                         :image   icons:*document-edit*
                                         :command (update-bookmark-clsr toplevel
                                                                        searchbox
                                                                        bookmarks))))
      (let ((searchbox-width (gui-goodies:quite-good-dialog-width)))
        (gui:configure (gui:listbox searchbox) :width searchbox-width))
      (gui:grid searchbox     0 0 :padx (* 2 +minimum-padding+) :pady (* 2 +minimum-padding+))
      (gui:grid delete-button 0 0 :sticky :s)
      (gui:grid edit-button   0 1 :sticky :s)
      (gui:grid buttons-frame 1 0 :sticky :s :padx (* 10 +minimum-padding+)))))
