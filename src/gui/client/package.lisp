;; tinmop: a multiprotocol client
;; Copyright © cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(defpackage :client-configuration
  (:use
   :cl
   :config
   :constants
   :text-utils
   :misc-utils)
  (:nicknames :gui-conf)
  (:local-nicknames (:re :cl-ppcre)
                    (:a  :alexandria))
  (:export
   :+client-conf-filename+
   :+client-sys-conf-filename+
   :load-config-file
   :config-icons-scaling
   :gemini-default-favicon
   :gemini-fetch-favicon-p
   :gemini-text-font-configuration
   :font-text-underlined
   :font-text-bold
   :font-text-italic
   :gemini-link-font-configuration
   :gemini-quote-font-configuration
   :gemini-h1-font-configuration
   :gemini-h2-font-configuration
   :gemini-h3-font-configuration
   :gemini-preformatted-text-font-configuration
   :gemini-link-prefix-to-gemini
   :gemini-link-prefix-to-other
   :gemini-link-prefix-to-http
   :gemini-quote-prefix
   :gemini-h1-prefix
   :gemini-h2-prefix
   :gemini-h3-prefix
   :gemini-bullet-prefix
   :gemini-preformatted-fg
   :gemini-link-colors
   :gemini-quote-colors
   :gemini-h1-colors
   :gemini-h2-colors
   :gemini-h3-colors
   :gemini-window-colors
   :gemini-preformatted-text-colors
   :gemini-quote-justification
   :gemini-h1-justification
   :gemini-h2-justification
   :gemini-h3-justification
   :gemini-preformatted-text-justification
   :config-toc-maximum-width
   :config-toc-minimum-width
   :config-toc-autoresize-p
   :toc-font-configuration
   :config-toc-show-p
   :get-keybinding
   :config-keybinding-tour-shuffle
   :config-keybinding-tour-manage
   :config-keybinding-tour-next
   :config-keybinding-gemtext-scaling-increase
   :config-keybinding-gemtext-scaling-decrease
   :config-keybinding-gemtext-scaling-reset
   :config-keybinding-gemtext-refresh
   :config-gemtext-padding
   :config-keybinding-bookmark-toggle
   :config-keybinding-bookmark-show
   :config-keybinding-bookmark-export
   :config-keybinding-bookmark-import
   :main-window-select-colors
   :emphasize-wrapped-asterisk-p
   :config-inline-scaling-ratio
   :config-stream-frame-show-p))

(defpackage :client-os-utils
  (:use
   :cl
   :config
   :constants)
  (:export
   :open-resource-with-external-program))

(defpackage :client-events
  (:use
   :cl
   :config
   :constants
   :text-utils
   :misc-utils)
  (:local-nicknames (:comm :json-rpc-communication)
                    (:re   :cl-ppcre)
                    (:a    :alexandria)
                    (:gui  :nodgui)
                    (:ev   :program-events))
  (:export
   :events-loop-running-p
   :stop-events-loop
   :start-events-loop
   :with-enqueue-request
   :enqueue-request-and-wait-results))

(defpackage :images
  (:use :cl
   :config
        :constants
   :misc
        :text-utils)
  (:local-nicknames (:comm       :json-rpc-communication)
                    (:re         :cl-ppcre)
                    (:a          :alexandria)
                    (:gui :nodgui)
                    (:gui.pixmap :nodgui.pixmap)
                    (:gui-utils  :nodgui.utils))
  (:export
   :+image-dir+
   :image-filename->filepath
   :load-image-path
   :load-image
   :load-images
   :*broken-image*))


(defpackage :icons
  (:use :cl
   :config
        :constants
   :misc
        :text-utils)
  (:local-nicknames (:comm       :json-rpc-communication)
                    (:re         :cl-ppcre)
                    (:a          :alexandria)
                    (:gui        :nodgui)
                    (:gui.pixmap :nodgui.pixmap)
                    (:gui-utils  :nodgui.utils))
  (:export
   :+icon-dir+
   :load-icons
   :*search*
   :*back*
   :*open-iri*
   :*open-tour*
   :*refresh*
   :*up*
   :*document-delete*
   :*document-add*
   :*document-accept*
   :*document-edit*
   :*folder*
   :*star-yellow*
   :*star-blue*
   :*arrow-up*
   :*arrow-down*
   :*cross*
   :*bus-go*
   :*dice*
   :*gemlog-subscribe*
   :*gemlog-unsubscribe*
   :*inline-images*
   :*text*
   :*profile*
   :*profile-disabled*
   :*toc*
   :*toc-disabled*))

(defpackage :validation
  (:use
   :cl
   :config
   :constants)
  (:local-nicknames (:comm      :json-rpc-communication)
                    (:re        :cl-ppcre)
                    (:a         :alexandria)
                    (:gui       :nodgui)
                    (:gui-utils :nodgui.utils))
  (:export
   :validator
   :regexp-validator
   :validate))

(defpackage :gui-goodies
  (:use :cl
   :config
        :constants
   :misc
        :text-utils)
  (:local-nicknames (:comm       :json-rpc-communication)
                    (:re         :cl-ppcre)
                    (:a          :alexandria)
                    (:gui        :nodgui)
                    (:gui-mw     :nodgui.mw)
                    (:gui-utils  :nodgui.utils)
                    (:gui-shapes :nodgui.shapes))
  (:export
   :*toplevel*
   :*gui-server*
   :*main-frame*
   :parse-color
   :make-font
   :gui-resize-grid-all
   :confirm-deletion
   :info-operation-completed
   :info-dialog
   :error-dialog
   :re-validate
   :with-re-validate
   :with-entry-text-validate
   :attach-tooltip
   :attach-tooltips
   :with-busy*
   :password-dialog
   :change-password-dialog
   :table-frame
   :tree
   :rows
   :quite-good-dialog-width
   :notify-request-error
   :with-notify-errors))

(defpackage :client-menu-command
  (:use
   :cl
   :config
   :constants
   :gui-goodies
   :text-utils
   :misc-utils)
  (:local-nicknames (:comm       :json-rpc-communication)
                    (:ev         :program-events)
                    (:cev        :client-events)
                    (:re         :cl-ppcre)
                    (:a          :alexandria)
                    (:gui        :nodgui)
                    (:gui-mw     :nodgui.mw)
                    (:gui-shapes :nodgui.shapes))
  (:export
   :help-about
   :quit
   :show-certificates-clsr
   :show-streams
   :show-bookmarks-clsr
   :export-bookmarks-clsr
   :import-bookmarks-clsr
   :manage-bookmarks-clsr
   :show-search-frame-clsr
   :show-tour-clsr
   :manage-gemlogs
   :show-page-source-clsr
   :search-gempub-library-clsr
   :make-gempub-clsr
   :install-gempub-clsr
   :manage-history-clsr))

(defpackage :client-certificates-window
  (:use
   :cl
   :config
   :constants
   :text-utils
   :misc-utils)
  (:local-nicknames (:comm       :json-rpc-communication)
                    (:re         :cl-ppcre)
                    (:a          :alexandria)
                    (:ev         :program-events)
                    (:cev        :client-events)
                    (:gui        :nodgui)
                    (:gui-mw     :nodgui.mw)
                    (:gui-shapes :nodgui.shapes)
                    (:menu       :client-menu-command))
  (:export
   :certificate-frame
   :tree
   :rows
   :init-window))

(defpackage :client-titan-window
  (:use
   :cl
   :config
   :constants
   :text-utils
   :misc-utils)
  (:local-nicknames (:comm       :json-rpc-communication)
                    (:re         :cl-ppcre)
                    (:a          :alexandria)
                    (:ev         :program-events)
                    (:cev        :client-events)
                    (:gui        :nodgui)
                    (:gui-mw     :nodgui.mw)
                    (:gui-shapes :nodgui.shapes)
                    (:menu       :client-menu-command))
  (:export
   :init-window))

(defpackage :client-tour-window
  (:use
   :cl
   :config
   :constants
   :text-utils
   :misc-utils)
  (:local-nicknames (:comm       :json-rpc-communication)
                    (:re         :cl-ppcre)
                    (:a          :alexandria)
                    (:ev         :program-events)
                    (:cev        :client-events)
                    (:gui        :nodgui)
                    (:gui-mw     :nodgui.mw)
                    (:gui-shapes :nodgui.shapes)
                    (:menu       :client-menu-command))
  (:export
   :enqueue-shuffle-tour
   :init-window))

(defpackage :client-stream-frame
  (:use
   :cl
   :config
   :constants
   :text-utils
   :misc-utils)
  (:local-nicknames (:cert-win   :client-certificates-window)
                    (:comm       :json-rpc-communication)
                    (:re         :cl-ppcre)
                    (:a          :alexandria)
                    (:ev         :program-events)
                    (:cev        :client-events)
                    (:gui        :nodgui)
                    (:gui-mw     :nodgui.mw)
                    (:gui-shapes :nodgui.shapes)
                    (:menu       :client-menu-command))
  (:export
   :init-frame))

(defpackage :client-bookmark-window
  (:use
   :cl
   :config
   :constants
   :text-utils
   :misc-utils)
  (:local-nicknames (:cert-win   :client-certificates-window)
                    (:comm       :json-rpc-communication)
                    (:re         :cl-ppcre)
                    (:a          :alexandria)
                    (:ev         :program-events)
                    (:cev        :client-events)
                    (:gui        :nodgui)
                    (:gui-mw     :nodgui.mw)
                    (:gui-shapes :nodgui.shapes)
                    (:menu       :client-menu-command))
  (:export
   :init-window
   :manage-bookmarks))

(defpackage :client-gemlog-window
  (:use
   :cl
   :config
   :constants
   :text-utils
   :misc-utils)
  (:local-nicknames (:cert-win   :client-certificates-window)
                    (:comm       :json-rpc-communication)
                    (:re         :cl-ppcre)
                    (:a          :alexandria)
                    (:ev         :program-events)
                    (:cev        :client-events)
                    (:gui        :nodgui)
                    (:gui-mw     :nodgui.mw)
                    (:gui-shapes :nodgui.shapes)
                    (:menu       :client-menu-command))
  (:export
   :subscribe-gemlog-or-notify-error
   :init-window))

(defpackage :client-gempub-window
  (:use
   :cl
   :config
   :constants
   :text-utils
   :misc-utils)
  (:local-nicknames (:cert-win   :client-certificates-window)
                    (:comm       :json-rpc-communication)
                    (:re         :cl-ppcre)
                    (:a          :alexandria)
                    (:ev         :program-events)
                    (:cev        :client-events)
                    (:gui        :nodgui)
                    (:gui-mw     :nodgui.mw)
                    (:gui-shapes :nodgui.shapes)
                    (:menu       :client-menu-command))
  (:export
   :init-window
   :create-gempub))

(defpackage :client-history-window
  (:use
   :cl
   :config
   :constants
   :text-utils
   :misc-utils)
  (:local-nicknames (:cert-win   :client-certificates-window)
                    (:comm       :json-rpc-communication)
                    (:re         :cl-ppcre)
                    (:a          :alexandria)
                    (:ev         :program-events)
                    (:cev        :client-events)
                    (:gui        :nodgui)
                    (:gui-mw     :nodgui.mw)
                    (:gui-shapes :nodgui.shapes)
                    (:menu       :client-menu-command))
  (:export
   :init-window))

(defpackage :client-search-frame
  (:use
   :cl
   :config
   :constants
   :text-utils
   :misc-utils)
  (:local-nicknames (:comm       :json-rpc-communication)
                    (:re         :cl-ppcre)
                    (:a          :alexandria)
                    (:ev         :program-events)
                    (:cev        :client-events)
                    (:gui        :nodgui)
                    (:gui-mw     :nodgui.mw)
                    (:gui-shapes :nodgui.shapes))
  (:export
   :init-frame))

(defpackage :client-scheduler
  (:use :cl
   :config
        :constants
   :misc
        :text-utils)
  (:local-nicknames (:comm      :json-rpc-communication)
                    (:re        :cl-ppcre)
                    (:a         :alexandria)
                    (:ev        :program-events)
                    (:gui       :nodgui)
                    (:gui-utils :nodgui.utils))
  (:export
   :start))

(defpackage :client-main-window
  (:use
   :cl
   :config
   :constants
   :text-utils
   :misc-utils)
  (:local-nicknames (:comm       :json-rpc-communication)
                    (:re         :cl-ppcre)
                    (:a          :alexandria)
                    (:ev         :program-events)
                    (:cev        :client-events)
                    (:gui        :nodgui)
                    (:gui-mw     :nodgui.mw)
                    (:gui-shapes :nodgui.shapes)
                    (:menu       :client-menu-command))
  (:export
   :status
   :+stream-status-streaming+
   :stop-stream-thread
   :stop-streaming-stream-thread
   :remove-db-stream
   :find-db-stream-url
   :open-local-path
   :init-main-window
   :set-address-bar-text
   :set-title-and-address-bar-text
   :set-bookmark-button-true
   :set-bookmark-button-false
   :clear-gemtext
   :print-info-message
   :make-internal-iri
   :internal-iri-bookmark
   :show-bookmarks-page
   :hide-autocomplete-candidates
   :gempub-metadata
   :gempub-mode-p
   :unset-gempub-mode
   :set-gempub-mode))
