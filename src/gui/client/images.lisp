;; tinmop: a multiprotocol client
;; Copyright © cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :images)

(a:define-constant +image-dir+    "/data/images/" :test #'string=)

(a:define-constant +broken-image+ "broken-image"  :test #'string=)

(defparameter *broken-image* nil)

(defun image-filename->filepath (filename &key
                                            (extension-regex "(?i)png$")
                                            (extension-appended-to-image-name ".png")
                                            (resources-subdir +image-dir+))
  (if (not (re:scan extension-regex filename))
      (res:get-data-file (fs:cat-parent-dir resources-subdir
                                            (strcat filename
                                                    extension-appended-to-image-name)))
      (res:get-data-file (fs:cat-parent-dir resources-subdir filename))))

(defun load-image-path (path)
  (with-open-file (stream path :element-type '(unsigned-byte 8))
    (let ((data (gui-utils:read-into-array stream (file-length stream))))
      (gui:image-scale (gui:make-image data)
                       (client-configuration:config-icons-scaling)))))

(defun load-image (filename)
  (load-image-path (image-filename->filepath filename)))

(defun load-images ()
  (let ((nodgui:*use-tk-for-decoding-png* t))
    (setf *broken-image* (load-image +broken-image+))))
