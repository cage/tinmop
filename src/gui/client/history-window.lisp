;; tinmop: a multiprotocol client
;; Copyright © cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :client-history-window)

(named-readtables:in-readtable nodgui.syntax:nodgui-syntax)

(defclass history-frame (gui:frame)
  ((interval-selection
    :initform nil
    :initarg  :interval-selection
    :accessor interval-selection)
   (links-selection
    :initform nil
    :initarg  :links-selection
    :accessor links-selection)))

(defun today-label ()
  (_ "Today"))

(defun yesterday-label ()
  (_ "Yesterday"))

(defun this-week-label ()
  (_ "This week"))

(defun last-two-weeks-label ()
  (_ "Latest two weeks"))

(defun last-month-label ()
  (_ "Last month"))

(defun last-year-label ()
  (_ "Last year"))

(defun all-labels ()
  (list (today-label)
        (yesterday-label)
        (this-week-label)
        (last-two-weeks-label)
        (last-month-label)
        (last-year-label)))

(defun slice-label->interval (label)
  (cond
    ((string= label (today-label))
     (values 1 0))
    ((string= label (yesterday-label))
     (values 2 1))
    ((string= label (this-week-label))
     (values 7 0))
    ((string= label (last-two-weeks-label))
     (values 15 0))
    ((string= label (last-month-label))
     (values 30 0))
    ((string= label (last-year-label))
     (values 365 0))))

(defmethod initialize-instance :after ((object history-frame) &key &allow-other-keys)
  (let ((interval-selection (make-instance 'gui:scrolled-listbox
                                           :master object
                                           :text (_ "Time slice")))
        (links-selection    (make-instance 'gui-mw:searchable-listbox
                                           :entry-label (_ "Filter (regexp): ")
                                           :key (lambda (a) (getf a :input))
                                           :remove-non-matching-p t
                                           :master object)))
    (setf (interval-selection object) interval-selection
          (links-selection    object) links-selection)
    (gui:listbox-append interval-selection (all-labels))
    (gui:grid interval-selection 0 0 :sticky :news)
    (gui:grid links-selection    0 1 :sticky :news)
    (gui:grid-rowconfigure    object :all :weight 1)
    (gui:grid-columnconfigure object 0 :weight 1)
    (gui:grid-columnconfigure object 1 :weight 2)))

(defun init-window (master main-window)
  (client-main-window:hide-autocomplete-candidates main-window)
  (gui:with-toplevel (toplevel :master master :title (_ "History window"))
    (gui:transient toplevel master)
    (let* ((screen-width-in-pixel (/ (gui:screen-width) 2))
           (frame                 (make-instance 'history-frame :master toplevel))
           (links-listbox         (links-selection frame)))
      (gui:configure toplevel :width screen-width-in-pixel)
      (gui:grid frame 0 0 :sticky :news)
      (gui:grid-columnconfigure toplevel :all :weight 1)
      (gui:grid-rowconfigure    toplevel :all :weight 1)
      (gui:bind (gui:listbox (interval-selection frame))
                #$<<ListboxSelect>>$
                (lambda (e)
                  (declare (ignore e))
                  (a:when-let ((selected (first (gui:listbox-get-selection-value
                                                 (interval-selection frame)))))
                    (ev:with-enqueued-process-and-unblock ()
                      (multiple-value-bind (from to)
                          (slice-label->interval selected)
                        (let ((rows (comm:make-request :gemini-history-rows
                                                       1
                                                       from
                                                       to)))
                          (gui:listbox-delete links-listbox)
                          (setf (gui-mw:data links-listbox) rows)
                          (gui:listbox-append (gui:listbox links-listbox)
                                              (gui-mw::get-searchable-listbox-data links-listbox))))))))
      (gui:bind (gui:listbox links-listbox)
                #$<<ListboxSelect>>$
                (lambda (e)
                  (declare (ignore e))
                  (a:when-let ((selected (first (gui:listbox-get-selection-value
                                                 links-listbox))))
                    (ev:with-enqueued-process-and-unblock ()
                      (client-main-window::open-iri selected main-window t))))))))
