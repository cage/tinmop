;; tinmop: a multiprotocol client
;; Copyright © cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :client-stream-frame)

(named-readtables:in-readtable nodgui.syntax:nodgui-syntax)

(defclass stream-frame (gui:frame)
  ((table
    :initform nil
    :initarg  :table
    :accessor table)))

(defclass stream-table (gui-goodies:table-frame) ())

(defun resync-rows (stream-table new-rows)
  (with-accessors ((tree gui-goodies:tree)
                   (rows gui-goodies:rows)) stream-table
    (gui:treeview-delete-all tree)
    (setf rows new-rows)
    (loop for row in rows do
      (a:when-let* ((iri                   (getf row :download-iri))
                    (stream-client-wrapper (client-main-window:find-db-stream-url iri))
                    (stream-status         (to-s (client-main-window::status
                                                  stream-client-wrapper)))
                    (tree-row              (make-instance 'gui:tree-item
                                                          :id    iri
                                                          :text  iri
                                                          :column-values
                                                          (list stream-status
                                                                (to-s (getf row
                                                                            :octet-count)))
                                                          :index gui:+treeview-last-index+)))
        (gui:treeview-insert-item tree :item tree-row)))
    (gui:treeview-refit-columns-width (gui-goodies:tree stream-table))
  stream-table))

(defun all-rows ()
  (let ((rows (cev:enqueue-request-and-wait-results :gemini-all-stream-info
                                                    1
                                                    ev:+standard-event-priority+)))
    (setf rows (sort rows
                     (lambda (a b) (string< (getf a :download-iri)
                                            (getf b :download-iri)))))
    rows))

(defun refresh-all-streams (stream-table &optional (all-rows-fn #'all-rows))
  (with-accessors ((tree gui-goodies:tree)
                   (rows gui-goodies:rows)) stream-table
    (let ((new-rows (funcall all-rows-fn)))
      (resync-rows stream-table new-rows)
      stream-table)))

(defun make-requests-all-rows ()
  (let ((rows (comm:make-request :gemini-all-stream-info 1)))
    (setf rows (sort rows
                     (lambda (a b) (string< (getf a :download-iri)
                                            (getf b :download-iri)))))
    rows))

(defmethod initialize-instance :after ((object stream-table) &key &allow-other-keys)
  (with-accessors ((tree gui-goodies:tree)) object
    (let ((treeview (make-instance 'gui:scrolled-treeview
                                   :master  object
                                   :columns (list (_ "Status")
                                                  (_ "Number of octets downloaded")))))
      (gui:treeview-heading treeview
                            gui:+treeview-first-column-id+
                            :text (_ "Address"))
      (setf tree treeview)
      (refresh-all-streams object)
      (gui:grid treeview  0 0 :sticky :news)
      (gui:grid-columnconfigure object :all :weight 1)
      (gui:grid-rowconfigure    object :all :weight 1))))

(defun delete-stream-clsr (stream-table)
  (with-accessors ((tree gui-goodies:tree)) stream-table
  (lambda ()
    (a:when-let* ((selections (gui:treeview-get-selection tree)))
      (loop for selection in selections do
        (let* ((url                   (gui:id selection))
               (stream-client-wrapper (client-main-window::find-db-stream-url url)))
          (when (eq (client-main-window:status stream-client-wrapper)
                    client-main-window:+stream-status-streaming+)
            (ev:with-enqueued-process-and-unblock ()
              (client-main-window:stop-streaming-stream-thread)))
          (ev:with-enqueued-process-and-unblock ()
            (client-main-window:remove-db-stream stream-client-wrapper)
            (comm:make-request :gemini-remove-stream 1 url))
          (let ((new-rows (all-rows)))
            (resync-rows stream-table new-rows))))))))

(defun revive-stream-clsr (stream-table main-window)
  (with-accessors ((tree gui-goodies:tree)) stream-table
    (lambda ()
      (client-main-window::interrupt-rendering main-window)
      (client-main-window::maybe-stop-streaming-stream-thread)
      (a:when-let* ((selections (gui:treeview-get-selection tree))
                    (selection  (first selections)))
        (let* ((url      (gui:id selection))
               (new-rows (all-rows)))
          (ev:with-enqueued-process-and-unblock ()
            (client-main-window::restart-rendering main-window)
            (client-main-window:set-title-and-address-bar-text main-window url)
            (resync-rows stream-table new-rows))
          (client-main-window::open-iri url main-window t))))))

(defun init-frame (parent-frame main-window)
  (let* ((wrapper-frame (make-instance 'stream-frame :master parent-frame))
         (table         (make-instance 'stream-table :master wrapper-frame))
         (buttons-frame (make-instance 'gui:frame  :master wrapper-frame))
         (reload-button (make-instance 'gui:button
                                       :master buttons-frame
                                       :image icons:*refresh*
                                       :command (lambda () (refresh-all-streams table))))
         (delete-button (make-instance 'gui:button
                                       :master  buttons-frame
                                       :image   icons:*document-delete*
                                       :command (delete-stream-clsr table)))
         (revive-button (make-instance 'gui:button
                                       :master  buttons-frame
                                       :image   icons:*document-accept*
                                       :command (revive-stream-clsr table main-window)))
         (close-button  (make-instance 'gui:button
                                       :image   icons:*cross*
                                       :master  buttons-frame
                                       :command
                                       (lambda () (gui:forget-pane parent-frame wrapper-frame)))))
    (setf (table wrapper-frame) table)
    (gui-goodies:attach-tooltips (reload-button (_ "refresh"))
                                 (delete-button (_ "delete selected stream"))
                                 (revive-button (_ "show selected stream"))
                                 (close-button  (_ "close")))
    (gui:grid buttons-frame 0 0 :sticky :w)
    (gui:grid table         0 1 :sticky :news)
    (gui:grid reload-button 0 0 :sticky :s)
    (gui:grid delete-button 1 0 :sticky :s)
    (gui:grid revive-button 2 0 :sticky :s)
    (gui:grid close-button  3 0 :sticky :s)
    (gui:grid-columnconfigure wrapper-frame 1 :weight 2)
    (gui:grid-rowconfigure    wrapper-frame :all :weight 1)
    (gui:bind (gui:treeview (gui-goodies:tree table))
                #$<Double-1>$
                (lambda (e)
                  (declare (ignore e))
                  (funcall (revive-stream-clsr table main-window))))
    wrapper-frame))
