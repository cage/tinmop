;; tinmop: a multiprotocol client
;; Copyright © cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :client-gempub-window)

(named-readtables:in-readtable nodgui.syntax:nodgui-syntax)

(defclass gempub-frame (gui-goodies:table-frame) ())

(defun build-column-values (title
                            author
                            year
                            description)
  (list title
        author
        year
        description))

(defmacro build-column-value-accessor (name index)
  (assert (>= index 0))
  (assert (symbolp name))
  `(defun ,(format-fn-symbol t "column-~a" name) (fields)
     (elt fields ,index)))

(build-column-value-accessor title         1)

(build-column-value-accessor author        2)

(build-column-value-accessor year          3)

(build-column-value-accessor description   4)

(db::gen-access-message-row description  :description)

(db::gen-access-message-row year         :published)

(db::gen-access-message-row cover        :cover)

(db::gen-access-message-row index        :index-file)

(db::gen-access-message-row path         :local-uri)

(defun resync-rows (gempub-frame new-rows)
  (with-accessors ((tree gui-goodies:tree)
                   (rows gui-goodies:rows)) gempub-frame
    (gui:treeview-delete-all tree)
    (setf rows new-rows)
    (loop for row in rows
          for index-count from 0 do
            (let* ((id          (text-utils:to-s (db:row-id row)))
                   (author      (db:row-author row))
                   (title       (db:row-title row))
                   (year        (row-year row))
                   (description (text-utils:ellipsize (row-description row)
                                                      50))
                   (tree-row (make-instance 'gui:tree-item
                                            :id            id
                                            :text          id
                                            :column-values (build-column-values title
                                                                                author
                                                                                year
                                                                                description)
                                            :index         gui:+treeview-last-index+)))
              (gui:treeview-insert-item tree :item tree-row)))
    (gui:treeview-refit-columns-width (gui-goodies:tree gempub-frame))
    gempub-frame))

(defmethod initialize-instance :after ((object gempub-frame) &key &allow-other-keys)
  (with-accessors ((tree gui-goodies:tree)
                   (rows gui-goodies:rows)) object
    (let ((treeview (make-instance 'gui:scrolled-treeview
                                    :master  object
                                    :pack    '(:side :top :expand t :fill :both)
                                    :columns (list (_ "Title")
                                                   (_ "Author")
                                                   (_ "Year")
                                                   (_ "Description")))))
      (setf tree treeview)
      (gui:treeview-heading tree
                            gui:+treeview-first-column-id+
                            :text (_ "ID"))
      (resync-rows object rows)
      object)))

(defun extract-gempub (gempub-path)
  (let ((temp-directory (fs:temporary-directory)))
    (os-utils:unzip-file gempub-path temp-directory)
    temp-directory))

(defun dump-index (temporary-directory filename content)
  (let ((path (fs:cat-parent-dir temporary-directory filename)))
    (with-open-file (stream
                     path
                     :direction :output
                     :if-exists :error
                     :if-does-not-exist :create)
      (write-sequence content stream))
    path))

(a:define-constant +cover-page-path+                       "/data/cover.gmi" :test #'string=)

(a:define-constant +cover-template-index-placeholder+      "{index}"         :test #'string=)

(a:define-constant +cover-template-cover-path-placeholder+ "{cover-path}"    :test #'string=)

(a:define-constant +cover-filename+                        "cover.gmi"       :test #'string=)

(defun construct-cover-page (output-dir filename index-path cover-path)
  (a:when-let* ((file-path (res:get-data-file +cover-page-path+))
                (template  (fs:slurp-file file-path)))
    (setf template (cl-ppcre:regex-replace-all +cover-template-index-placeholder+
                                               template
                                               index-path))
    (setf template (cl-ppcre:regex-replace-all +cover-template-cover-path-placeholder+
                                               template
                                               cover-path))
    (dump-index output-dir filename template)))

(defun make-gempub-index (gempub-metadata)
  (let ((gempub-content-directory (extract-gempub (row-path gempub-metadata))))
    (values
     (cond
       ((and (row-cover gempub-metadata)
             (row-index gempub-metadata))
        (construct-cover-page gempub-content-directory
                              +cover-filename+
                              (row-index gempub-metadata)
                              (row-cover gempub-metadata)))
       ((row-index gempub-metadata)
        (row-index gempub-metadata))
       (t
        gempub-content-directory))
     (fs:cat-parent-dir gempub-content-directory "/"))))

(defun open-gemini-toc (main-window gempub-metadata)
  (client-main-window:set-title-and-address-bar-text main-window
                                                     (getf gempub-metadata :book-directory))
  (client-main-window::open-iri (getf gempub-metadata :index-file)
                                main-window
                                nil)
  (ev:with-enqueued-process-and-unblock ()
    (client-main-window::inline-all-images main-window)))

(defun open-gempub-clsr (main-window gempub-frame)
  (lambda (e)
    (declare (ignore e))
    (gui-goodies:with-notify-errors
      (a:when-let* ((treeview-gempubs (gui-goodies:tree gempub-frame))
                    (selection (a:first-elt (gui:treeview-get-selection treeview-gempubs)))
                    (id        (gui:id selection))
                    (row       (cev:enqueue-request-and-wait-results :gempub-file-id->row
                                                                     1
                                                                     ev:+standard-event-priority+
                                                                     id)))
        (multiple-value-bind (path book-directory)
            (make-gempub-index row)
          (setf (getf row :index-file) path)
          (setf (getf row :book-directory) book-directory)
          (client-main-window:set-gempub-mode main-window row)
          (open-gemini-toc main-window row))))))

(defun init-window (master main-window query-results)
  (client-main-window:hide-autocomplete-candidates main-window)
  (gui:with-toplevel (toplevel :master master :title (_ "Query results"))
    (let* ((table (make-instance 'gempub-frame
                                 :master toplevel
                                 :rows   query-results)))
      (gui:grid table 0 0 :sticky :nwe)
      (gui:bind (gui:treeview (gui-goodies:tree table))
                #$<<TreeviewSelect>>$
                (open-gempub-clsr main-window table))
      (gui:bind (gui:treeview (gui-goodies:tree table))
                #$<Delete>$
                (lambda (event)
                  (declare (ignore event))
                  (with-accessors ((tree gui-goodies:tree)) table
                    (a:when-let* ((selection (a:first-elt (gui:treeview-get-selection tree)))
                                  (delete    (gui:ask-yesno (_ "Are you sure? This operation is irreversible.")
                                                            :title  (_ "Attention")
                                                            :parent main-window))
                                  (id        (gui:id selection))
                                  (row       (cev:enqueue-request-and-wait-results :gempub-file-id->row
                                                                                   1
                                                                                   ev:+standard-event-priority+
                                                                                   id)))

                      (fs:delete-file-if-exists (row-path row))
                      (gui-goodies:info-operation-completed main-window)))))
      (gui:focus (gui:treeview (gui-goodies:tree table)))
      (gui:transient toplevel master))))

(defun %make-gempub (gemtext-directory title index-file author language description published-date license copyright cover-file)
  (let ((index-filename (fs:path-last-element index-file))
        (cover-filename (fs:path-last-element cover-file)))
    (fs:copy-a-file index-file (fs:cat-parent-dir gemtext-directory index-filename) :overwrite t)
    (fs:copy-a-file cover-file (fs:cat-parent-dir gemtext-directory cover-filename) :overwrite t)
    (with-open-file (stream
                     (fs:cat-parent-dir gemtext-directory gempub:+metadata-entry-name+)
                   :direction :output
                   :if-does-not-exist :create
                   :if-exists :supersede)
      (format stream
              "title: ~a~%index: ~a~%author: ~a~%language: ~a~%description: ~a~%published: ~a~%license: ~a~%copyright: ~a~%cover: ~a~%"
              title
              index-filename
              author
              language
              description
              published-date
              license
              copyright
              cover-filename))
    (os-utils:zip-directory gemtext-directory
                            :extension gempub:+gempub-file-extension+
                            :flat t)))

(defun create-gempub (gemtext-dir)
  (gui:with-modal-toplevel (toplevel :title (_ "Add Metadata"))
    (let* ((toplevel-widget (gui:modal-toplevel-root-widget toplevel))
           (summary           (make-instance 'gui:label
                                             :master toplevel-widget
                                             :text (_ "Provide the information as metadata for your book then presso OK button: fields marched with \"*\" are mandatory")
                                             :grid '(0 0)))
           (frame             (make-instance 'gui:frame
                                             :master toplevel-widget
                                             :grid '(1 0 :sticky :news)))
           (title-label       (make-instance 'gui:label
                                             :master frame
                                             :text (_ "Title*")
                                             :grid '(0 0)))
           (title-entry       (make-instance 'gui:entry
                                             :master frame
                                             :grid '(1 0)))
           (index-label       (make-instance 'gui:label
                                             :master frame
                                             :text ""
                                             :grid '(2 1)))
           (index-button      (make-instance 'gui:button
                                             :master frame
                                             :text (_ "Choose index file")
                                             :grid '(2 0)
                                             :command
                                             (lambda ()
                                               (let* ((file-types `((,(_ "Gempub") "*.gmi")))
                                                      (file (gui:get-open-file :file-types
                                                                               file-types
                                                                               :initial-dir
                                                                               gemtext-dir
                                                                               :parent
                                                                               toplevel-widget)))
                                                 (setf (gui:text index-label) file)))))
           (author-label      (make-instance 'gui:label
                                             :master frame
                                             :text (_ "Author")
                                             :grid '(3 0)))
           (author-entry      (make-instance 'gui:entry
                                             :master frame
                                             :grid '(4 0)))
           (language-label    (make-instance 'gui:label
                                             :master frame
                                             :text (_ "Language")
                                             :grid '(5 0)))
           (language-entry    (make-instance 'gui:entry
                                             :master frame
                                             :grid '(6 0)))
           (description-label (make-instance 'gui:label
                                             :master frame
                                             :text (_ "Description")
                                             :grid '(7 0)))
           (description-entry (make-instance 'gui:entry
                                             :master frame
                                             :grid '(8 0)))
           (published-label   (make-instance 'gui:label
                                             :master frame
                                             :text (_ "Published* (format: YYYY-MM-DD)")
                                             :grid '(9 0)))
           (published-entry   (make-instance 'gui:entry
                                             :master frame
                                             :grid '(10 0)))

           (license-label     (make-instance 'gui:label
                                             :master frame
                                             :text (_ "License")
                                             :grid '(11 0)))
           (license-entry     (make-instance 'gui:entry
                                             :master frame
                                             :grid '(12 0)))
           (copyright-label   (make-instance 'gui:label
                                             :master frame
                                             :text (_ "Copyright")
                                             :grid '(13 0)))
           (copyright-entry   (make-instance 'gui:entry
                                             :text "©…"
                                             :master frame
                                             :grid '(14 0)))
           (cover-label       (make-instance 'gui:label
                                             :master frame
                                             :text (_ "")
                                             :grid '(15 1)))
           (cover-button      (make-instance 'gui:button
                                             :master frame
                                             :text (_ "Choose a cover file")
                                             :grid '(15 0)
                                             :command
                                             (lambda ()
                                               (let* ((file-types `((,(_ "Jpeg files") "*.jpeg")
                                                                    (,(_ "Jpeg files") "*.jpg")
                                                                    (,(_ "PNG files") "*.png")))
                                                      (file (gui:get-open-file :file-types
                                                                               file-types
                                                                               :initial-dir
                                                                               gemtext-dir
                                                                               :parent
                                                                               toplevel-widget)))
                                                 (setf (gui:text cover-label) file)))))

           (ok-button-cb      (lambda ()
                                (cond
                                  ((not (ignore-errors (misc:parse-timestring (gui:text published-entry))))
                                   (gui-goodies:error-dialog toplevel-widget (_ "The format of the puiblished data is invalid")))
                                  ((text-utils:string-empty-p (gui:text title-entry))
                                   (gui-goodies:error-dialog toplevel-widget
                                                             (_ "The title is mandatory")))
                                  (t
                                   (gui-goodies:with-busy* (toplevel-widget)
                                     (%make-gempub gemtext-dir
                                                   (gui:text title-entry)
                                                   (gui:text index-label)
                                                   (gui:text author-entry)
                                                   (gui:text language-entry)
                                                   (gui:text description-entry)
                                                   (gui:text published-entry)
                                                   (gui:text license-entry)
                                                   (gui:text copyright-entry)
                                                   (gui:text cover-label))
                                     (gui:exit-from-modal-toplevel toplevel))))))
           (ok-button         (make-instance 'gui:button
                                             :grid '(2 0)
                                             :master  toplevel-widget
                                             :text    (_ "OK")
                                             :command ok-button-cb))
           (cancel-button     (make-instance 'gui:button
                                             :grid '(2 1)
                                             :master  toplevel-widget
                                             :text    (_ "Cancel")
                                             :command
                                             (lambda ()
                                               (gui:exit-from-modal-toplevel toplevel)))))
    (declare (ignore summary
                     title-label
                     index-button
                     author-label
                     language-label
                     description-label
                     published-label
                     license-label
                     copyright-label
                     cover-button
                     ok-button
                     cancel-button)))))
