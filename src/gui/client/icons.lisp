;; tinmop: a multiprotocol client
;; Copyright © cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :icons)

(a:define-constant +icon-dir+         "/data/icons/"               :test #'string=)

(a:define-constant +search+             "fmw_search"               :test #'string=)

(a:define-constant +back+               "fmw_back"                 :test #'string=)

(a:define-constant +go+                 "fmw_go"                   :test #'string=)

(a:define-constant +open-tour+          "fmw_open_tour"            :test #'string=)

(a:define-constant +refresh+            "fmw_refresh"              :test #'string=)

(a:define-constant +up+                 "fmw_uparrow"              :test #'string=)

(a:define-constant +document-delete+    "fmw_document-delete"      :test #'string=)

(a:define-constant +document-add+       "fmw_document-add"         :test #'string=)

(a:define-constant +document-accept+    "fmw_document-accept"      :test #'string=)

(a:define-constant +document-edit+      "fmw_document-edit"        :test #'string=)

(a:define-constant +folder+             "fmw_folder"               :test #'string=)

(a:define-constant +star-yellow+        "fmw_star-yellow.png"      :test #'string=)

(a:define-constant +star-blue+          "fmw_star-blue.png"        :test #'string=)

(a:define-constant +arrow-up+           "fmw_arrow-up"             :test #'string=)

(a:define-constant +arrow-down+         "fmw_arrow-down"           :test #'string=)

(a:define-constant +cross+              "fmw_cross"                :test #'string=)

(a:define-constant +bus-go+             "fmw_bus-go"               :test #'string=)

(a:define-constant +dice+               "fmw_dice"                 :test #'string=)

(a:define-constant +gemlog-subscribe+   "fmw_rss-add.png"          :test #'string=)

(a:define-constant +gemlog-unsubscribe+ "fmw_rss-delete.png"       :test #'string=)

(a:define-constant +inline-images+      "fmw_two-pictures.png"     :test #'string=)

(a:define-constant +text+               "fmw_text.png"             :test #'string=)

(a:define-constant +profile+            "fmw_profile.png"          :test #'string=)

(a:define-constant +toc+                "fmw_toc.png"              :test #'string=)

(defparameter *search*             nil)

(defparameter *back*               nil)

(defparameter *open-iri*           nil)

(defparameter *open-tour*          nil)

(defparameter *refresh*            nil)

(defparameter *up*                 nil)

(defparameter *document-delete*    nil)

(defparameter *document-add*       nil)

(defparameter *document-accept*    nil)

(defparameter *document-edit*      nil)

(defparameter *folder*             nil)

(defparameter *star-yellow*        nil)

(defparameter *star-blue*          nil)

(defparameter *arrow-up*           nil)

(defparameter *arrow-down*         nil)

(defparameter *cross*              nil)

(defparameter *bus-go*             nil)

(defparameter *dice*               nil)

(defparameter *gemlog-subscribe*   nil)

(defparameter *gemlog-unsubscribe* nil)

(defparameter *inline-images*      nil)

(defparameter *text*               nil)

(defparameter *profile*            nil)

(defparameter *profile-disabled*   nil)

(defparameter *toc*                nil)

(defparameter *toc-disabled*       nil)

(defun icon-filename->filepath (filename)
  (images:image-filename->filepath filename
                                   :resources-subdir +icon-dir+))

(defun load-icon (filename)
  (images:load-image-path (icon-filename->filepath filename)))

(defun disable-icon (filename)
  (let ((pixmap (gui.pixmap:slurp-pixmap 'gui.pixmap:png
                                         (icon-filename->filepath filename))))
    (gui.pixmap:to-disabled pixmap)
    (gui:image-scale (gui:make-image (gui.pixmap:encode-base64 pixmap))
                     (client-configuration:config-icons-scaling))))

(defun load-icons ()
  (let ((nodgui:*use-tk-for-decoding-png* t))
    (setf *search*             (load-icon +search+))
    (setf *back*               (load-icon +back+))
    (setf *open-iri*           (load-icon +go+))
    (setf *open-tour*          (load-icon +open-tour+))
    (setf *refresh*            (load-icon +refresh+))
    (setf *up*                 (load-icon +up+))
    (setf *document-delete*    (load-icon +document-delete+))
    (setf *document-add*       (load-icon +document-add+))
    (setf *document-accept*    (load-icon +document-accept+))
    (setf *document-edit*      (load-icon +document-edit+))
    (setf *folder*             (load-icon +folder+))
    (setf *star-yellow*        (load-icon +star-yellow+))
    (setf *star-blue*          (load-icon +star-blue+))
    (setf *arrow-up*           (load-icon +arrow-up+))
    (setf *arrow-down*         (load-icon +arrow-down+))
    (setf *cross*              (load-icon +cross+))
    (setf *bus-go*             (load-icon +bus-go+))
    (setf *dice*               (load-icon +dice+))
    (setf *gemlog-subscribe*   (load-icon +gemlog-subscribe+))
    (setf *gemlog-unsubscribe* (load-icon +gemlog-unsubscribe+))
    (setf *inline-images*      (load-icon +inline-images+))
    (setf *text*               (load-icon +text+))
    (setf *profile*            (load-icon +profile+))
    (setf *profile-disabled*   (disable-icon +profile+))
    (setf *toc*                (load-icon +toc+))
    (setf *toc-disabled*       (disable-icon +toc+))))
