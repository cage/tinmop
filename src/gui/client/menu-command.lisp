;; tinmop: a multiprotocol client
;; Copyright © cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :client-menu-command)

(defun help-about ()
  (let ((master gui-goodies:*toplevel*))
    (gui:with-toplevel (toplevel :master master :title (_ "About"))
      (gui:transient toplevel master)
      (let* ((font         (gui-goodies:make-font "sans" 12 nil nil nil))
             (link-font    (gui-goodies:make-font "sans" 12 :bold nil t))
             (text-widget  (make-instance 'gui:scrolled-text
                                          :font   font
                                          :master toplevel
                                          :cursor gui:+standard-cursor+
                                          :use-horizontal-scrolling-p nil
                                          :read-only t))
             (button-frame (make-instance 'gui:frame :master toplevel))
             (close-button (make-instance 'gui:button
                                          :image   icons:*cross*
                                          :master  button-frame
                                          :command (lambda ()
                                                     (gui:exit-from-toplevel toplevel)))))
        (setf (gui:text text-widget) (format nil +help-about-message-no-uri+))
        (gui:make-link-button text-widget
                              `(:line 1 :char 0)
                              `(:line 1 :char :end)
                              link-font
                              (gui-goodies:parse-color "blue")
                              (gui-goodies:parse-color "white")
                              (lambda ()
                                (os-utils:xdg-open +package-url+)))
        (gui:append-line text-widget "For bug report please point your browser to:")
        (gui:append-line text-widget "")
        (gui:append-text text-widget +issue-tracker+)
        (let ((last-line (gui:maximum-lines-number text-widget)))
          (gui:make-link-button text-widget
                                `(:line ,last-line :char 0)
                                (gui:make-indices-end)
                                link-font
                                (gui-goodies:parse-color "blue")
                                (gui-goodies:parse-color "white")
                                (lambda ()
                                  (os-utils:xdg-open +issue-tracker+))))
        (gui-goodies:attach-tooltips (close-button (_ "Close")))
        (gui:grid text-widget  0 0 :sticky :news)
        (gui:grid button-frame 1 0 :sticky :s)
        (gui:grid close-button 0 0 :sticky :n)
        (gui:wait-complete-redraw)))))

(defun quit ()
  (gui:exit-nodgui)
  (client-events:stop-events-loop)
  (comm:close-server))

(defun show-certificates-clsr (main-window)
  (lambda ()
    (let ((master gui-goodies:*toplevel*))
      (client-certificates-window:init-window master main-window))))

(defun show-streams ()
  (let* ((master gui-goodies:*main-frame*)
         (stream-frame (client-main-window::stream-frame master))
         (main-frame   (client-main-window::main-paned-frame master)))
    (when (not (gui:paned-widget-p main-frame stream-frame))
      (gui:add-pane main-frame stream-frame))))

(defun show-bookmarks-clsr (main-window)
  (lambda ()
    (client-main-window:show-bookmarks-page main-window)))

(defun export-bookmarks-clsr (main-window)
  (lambda ()
    (a:when-let ((file-path (gui:get-save-file :file-types (list (list (_ "Gemtext files")
                                                                       "*.gmi"))
                                               :title      (_ "Choose file for exporting…")
                                               :parent     main-window)))
      (if (fs:directory-exists-p file-path)
          (error-dialog main-window (format nil "~a is a directory" file-path))
          (ev:with-enqueued-process-and-unblock ()
            (let ((page (comm:make-request :gemini-generate-raw-bookmark-page 1)))
              (with-open-file (stream file-path
                                      :direction         :output
                                      :if-exists         :supersede
                                      :if-does-not-exist :create)
                (write-sequence page stream)
                (info-operation-completed main-window))))))))

(defun import-bookmarks-clsr (main-window)
  (lambda ()
    (a:when-let ((file-path (gui:get-open-file :file-types (list (list (_ "Gemtext files")
                                                                       "*.gmi"))
                                               :title      (_ "Choose file for importing…")
                                               :parent     main-window)))
      (cond
        ((fs:directory-exists-p file-path)
         (error-dialog main-window (format nil "~a is a directory" file-path)))
        ((not (fs:file-exists-p file-path))
         (error-dialog main-window (format nil "file ~a does not exists" file-path)))
        (t
         (let* ((page (fs:slurp-file file-path))
                (parsed-page (cev:enqueue-request-and-wait-results :gemini-parse-string
                                                                   1
                                                                   ev:+standard-event-priority+
                                                                   page))
                (errors      '()))
           (loop with current-category = (ui:localized-default-string)
                 for line in parsed-page do
                   (cond
                     ((string= (getf line :type) "h2")
                      (setf current-category (getf line :line (ui:localized-default-string))))
                     ((string= (getf line :type) "a")
                      (let ((iri         (getf line :href))
                            (description (getf line :line)))
                        (if (gemini-parser:gemini-iri-p iri)
                            (ev:with-enqueued-process-and-unblock ()
                              (comm:make-request :gemini-bookmark-delete 1 iri)
                              (comm:make-request :gemini-bookmark-add
                                                 1
                                                 iri
                                                 current-category
                                                 description))
                            (push (format nil "unable to parse ~a" iri) errors))))))
           (ev:with-enqueued-process-and-unblock ()
             (if errors
                 (error-dialog main-window (format nil "~{~a~%~}" errors))
                 (info-operation-completed main-window)))))))))

(defun manage-bookmarks-clsr (main-window)
  (lambda ()
    (client-bookmark-window:manage-bookmarks main-window)))

(defun show-search-frame-clsr (main-window)
  (lambda ()
    (gui:grid (client-main-window::search-frame main-window) 2 0 :sticky :news)
    (gui:focus (client-search-frame::entry (client-main-window::search-frame main-window)))))

(defun show-tour-clsr (main-window)
  (lambda ()
    (let ((master gui-goodies:*toplevel*))
      (client-tour-window:init-window master main-window))))

(defun manage-gemlogs ()
  (let ((master      gui-goodies:*toplevel*)
        (main-window gui-goodies:*main-frame*))
    (client-gemlog-window:init-window master main-window)))

(defun show-page-source-clsr (main-window)
  (lambda ()
    (a:when-let ((iri (iri:iri-parse (client-main-window::get-address-bar-text main-window)
                                     :null-on-error t)))
      (setf (iri:scheme iri) +internal-scheme-view-source+)
      (client-main-window:set-title-and-address-bar-text main-window (to-s iri))
      (client-main-window::open-iri (to-s iri) main-window nil))))

(defun search-gempub-library-clsr (main-window)
  (lambda ()
    (gui-goodies:with-notify-errors
      (let* ((query (gui-mw:text-input-dialog main-window
                                              (_ "Search library")
                                              (_ "Search the books that matches the criteria below, see the manpage for details about the query language")
                                              :text "where…"
                                              :button-message (_ "OK")))
             (table-results (cev:enqueue-request-and-wait-results :gempub-search
                                                                  1
                                                                  ev:+standard-event-priority+
                                                                  query)))
        (client-gempub-window:init-window main-window main-window table-results)))))

(defun make-gempub-clsr (main-window)
  (declare (ignore main-window))
  (lambda ()
    (let ((directory (gui:choose-directory :title (_ "Choose directory with the gempub sources"))))
      (when (text-utils:string-not-empty-p directory)
        (client-gempub-window:create-gempub directory)))))

(defun install-gempub-clsr (main-window)
  (lambda ()
    (let* ((input-file-path (gui:get-open-file :title (_ "Choose gempub file")
                                               :file-types '(("Gempub files" "*.gpub"))))
           (output-file     (fs:cat-parent-dir (swconf:gempub-library-directory)
                                           (fs:path-last-element input-file-path))))
      (when (and (text-utils:string-not-empty-p input-file-path)
                 (fs:file-exists-p input-file-path)
                 (zip-info:zip-file-p input-file-path))
        (let ((overwrite (or (not (fs:file-exists-p output-file))
                             (gui:ask-yesno (_ "A file with this name exists in your library: overwrite?")
                                            :title  (_ "Question")
                                            :parent main-window))))
          (fs:copy-a-file input-file-path
                          output-file
                          :overwrite overwrite))))))

(defun manage-history-clsr (main-window)
  (lambda ()
    (let ((master gui-goodies:*toplevel*))
      (client-history-window:init-window master main-window))))
