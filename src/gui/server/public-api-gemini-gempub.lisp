;; tinmop: a multiprotocol client
;; Copyright © cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :json-rpc-communication)

(defclass gemini-gempub-search-results (box) ())

(defmethod yason:encode ((object gemini-gempub-search-results)
                         &optional (stream *standard-output*))
  (let ((table (unbox object)))
    (encode-flat-array-of-plists table stream)))

(defun gempub-search (query)
  (make-instance  'gemini-gempub-search-results
                  :contents (gempub:parse-search-gempub query)))

(defun gempub-synchronize-library ()
  (gempub:sync-library :notify nil)
  t)

(defun gempub-file-p (path)
  (gempub:gempub-file-p path :ignore-errors t))

(defun gempub-index-path (gempub-filepath)
  (when (gempub-file-p gempub-filepath)
    (let ((temp-directory (fs:temporary-directory)))
      (os-utils:unzip-file gempub-filepath temp-directory)
      (let* ((library-entry (db:gempub-metadata-find gempub-filepath))
             (index-file    (db:row-index-file library-entry)))
        (if index-file
            (fs:cat-parent-dir temp-directory index-file)
            temp-directory)))))

(defun gempub-file-id->path (id)
  (db:gempub-metadata-id->path id))

(defclass gemini-gempub-row (box) ())

(defmethod yason:encode ((object gemini-gempub-row)
                         &optional (stream *standard-output*))
  (let ((row (unbox object))
        (json:*symbol-encoder* #'json:encode-symbol-as-lowercase)
        (yason:*list-encoder* #'yason:encode-plist)
        (json:*symbol-key-encoder* #'json:encode-symbol-as-lowercase))
    (yason:with-output (stream)
      (json:encode row))))

(defun gempub-file-id->row (id)
  (make-instance 'gemini-gempub-row
                 :contents (db:gempub-metadata-id->row id)))
