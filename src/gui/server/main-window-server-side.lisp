;; tinmop: a multiprotocol client
;; Copyright © cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :json-rpc-communication)

(defclass gemini-window (metadata-container)
  ((links-tour
    :initform '()
    :initarg :links-tour
    :accessor links-tour)))

(defgeneric shuffle-tour (object))

(defgeneric add-tour-link (object link))

(defgeneric pop-tour-link (object))

(defgeneric delete-tour-link-element (object handle))

(defgeneric clear-tour-link (object))

(defmethod shuffle-tour ((object gemini-window))
  (setf (links-tour object) (misc:shuffle (links-tour object)))
  object)

(defmethod add-tour-link ((object gemini-window) (link gemini-parser:gemini-link))
  (with-accessors ((links-tour links-tour)) object
    (when (not (find link
                     links-tour
                     :test (lambda (a b)
                             (string= (gemini-parser:target a)
                                      (gemini-parser:target b)))))
      (a:reversef links-tour)
      (push link links-tour)
      (a:reversef links-tour)
      object)))

(defmethod pop-tour-link ((object gemini-window))
  (with-accessors ((links-tour links-tour)) object
    (when links-tour
      (pop links-tour))))

(defmethod delete-tour-link-element ((object gemini-window) url)
  (with-accessors ((links-tour links-tour)) object
    (setf links-tour (remove-if (lambda (a)
                                     (string= (gemini-parser:target a)
                                              url))
                                links-tour))
    object))

(defmethod clear-tour-link ((object gemini-window))
  (with-accessors ((links-tour links-tour)) object
    (setf links-tour '())))

(defparameter *gemini-window* nil)

(defun init-gemini-window ()
  (setf *gemini-window*
        (make-instance 'gemini-window))
  (gw:maybe-initialize-metadata *gemini-window*))

(defmethod gemini-window-p ((win gemini-window))
  t)

(defun gemini-current-url ()
  (gw:current-gemini-url *gemini-window*))

(defun gemini-pop-url-from-history ()
  (gw:pop-url-from-history *gemini-window*))

(defun gemini-push-url-to-history (iri)
  (gw:push-url-to-history *gemini-window* iri)
  t)

(defun iri-to-parent-path (iri)
  (iri:iri-to-parent-path iri))
