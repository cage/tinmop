;; tinmop: a multiprotocol client
;; Copyright © cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :json-rpc-communication)

(defun tour-shuffle ()
  (progn
    (shuffle-tour *gemini-window*)
    t))

(defun tour-add-link (link-value link-label)
  (progn
    (add-tour-link *gemini-window*
                   (make-instance 'gemini-parser:gemini-link
                                  :name  link-label
                                  :target link-value))
    t))

(defclass popped-tour-link (box) ())

(defmethod yason:encode ((object popped-tour-link) &optional (stream *standard-output*))
  (let ((json:*symbol-encoder* #'json:encode-symbol-as-lowercase)
        (yason:*list-encoder* #'yason:encode-plist)
        (json:*symbol-key-encoder* #'json:encode-symbol-as-lowercase))
    (yason:encode-plist (unbox object) stream)))

(defun tour-pop-link ()
  (a:when-let ((link (pop-tour-link *gemini-window*)))
    (make-instance 'popped-tour-link
                   :contents  (list :link-value (gemini-parser:target link)
                                    :link-label (gemini-parser:name   link)))))

(defun tour-delete-link (url)
  (progn
    (delete-tour-link-element *gemini-window* url)
    t))

(defun clear-tour ()
  (clear-tour-link *gemini-window*))

(defclass tour (box) ())

(defmethod yason:encode ((object tour) &optional (stream *standard-output*))
  (let ((json:*symbol-encoder* #'json:encode-symbol-as-lowercase)
        (yason:*list-encoder* #'yason:encode-plist)
        (json:*symbol-key-encoder* #'json:encode-symbol-as-lowercase)
        (tour (unbox object)))
      (yason:with-output (stream)
        (yason:with-array ()
          (loop for tour-link in tour do
            (yason:encode-array-elements
              (list :name
                    (gemini-parser:name tour-link)
                    :target
                    (gemini-parser:target tour-link))))))))

(defun tour-all-links ()
  (make-instance 'tour :contents (links-tour *gemini-window*)))
