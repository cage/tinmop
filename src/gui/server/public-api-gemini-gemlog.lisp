;; tinmop: a multiprotocol client
;; Copyright © cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :json-rpc-communication)

(defclass iri-complete-response (box) ())

(defun gemini-gemlog-subscribe (iri)
  (gemini-subscription::subscribe iri))

(defun gemini-gemlog-subscribed-p (iri)
  (if (db:gemini-gemlog-subscribed-p iri)
      t
      nil))

(defclass gemini-gemlog-subscriptions (box) ())

(defmethod yason:encode ((object gemini-gemlog-subscriptions) &optional (stream *standard-output*))
  (encode-flat-array-of-plists (unbox object) stream))

(defun gemini-gemlog-all-subscription ()
  (let ((table (db:gemini-all-subscriptions)))
    (make-instance 'gemini-gemlog-subscriptions
                   :contents table)))

(defun gemini-gemlog-unsubscribe (iri)
  (db:gemini-cancel-subscription iri))

(defun gemini-gemlog-entries-page (iri title subtitle)
  (let* ((table (db:gemlog-entries iri))
         (page  (program-events::build-gemlog-page title
                                                   subtitle
                                                   table)))
    (gemini-parse-string page)))

(defclass gemini-gemlog-entries (box) ())

(defmethod yason:encode ((object gemini-gemlog-entries) &optional (stream *standard-output*))
  (encode-flat-array-of-plists (unbox object) stream))

(defun gemini-gemlog-entries (iri)
  (let ((table (db:gemlog-entries iri)))
    (make-instance 'gemini-gemlog-entries
                   :contents table)))

(defun gemini-gemlog-refresh-subscription (gemlog-url)
  (handler-case
      (list (cons "url" gemlog-url)
            (cons "new-posts"
                  (gemini-subscription:refresh-subscription-low-level gemlog-url)))
    (condition (e)
      (error (format nil (_ "Error getting posts from ~s: ~a") gemlog-url e)))))

(defclass gemini-gemlog-subscriptions-refreshed (box) ())

(defmethod yason:encode ((object gemini-gemlog-subscriptions-refreshed)
                         &optional (stream *standard-output*))
  (encode-flat-array-of-plists (unbox object) stream))

(defun gemini-gemlog-refresh-all-subscriptions ()
  (let ((all-subscribed-gemlogs (mapcar #'db:row-url (db:gemini-all-subscriptions))))
    (loop for subscription in all-subscribed-gemlogs
          collect
          (handler-case
              (gemini-gemlog-refresh-subscription subscription)
            (usocket:ns-host-not-found-error (e)
              (error (_ "unable to connect to \"~a\": host not found")
                     (usocket:host-or-ip e)))
            (error (e)
              (error e))))))

(defun gemini-purge-gemlog-entries ()
  (db:purge-seen-gemlog-entries)
  t)

(defun gemini-mark-gemlog-post-read (iri)
  (db:gemlog-mark-as-seen iri))
