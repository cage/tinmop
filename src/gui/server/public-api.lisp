;; tinmop: a multiprotocol client
;; Copyright © cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :json-rpc-communication)

(defun make-rpc-parameters (&rest params)
  (loop for (a b) on params by 'cddr collect (cons a b)))

(defmacro gen-rpc (public-function-name function-symbol &rest parameters)
  `(rpc:register-function ,public-function-name
                          ,function-symbol
                          (make-rpc-parameters ,@parameters)))

(defun quit-program ()
  (fs:clean-temporary-directories)
  (fs:clean-temporary-files)
  (db-utils:close-db)
  (stop-server))

(defun purge-history ()
  (db:purge-history)
  t)

(defun invalidate-cached-value (cache-key)
  (db:cache-invalidate cache-key)
  t)

(defmacro prepare-rpc (&body body)
  `(let ((rpc:*function-db* '()))
     (gen-rpc "complete-net-address"
              'complete-net-address
              "hint" 0)
     (gen-rpc "gemini-url-needs-proxy-p"
              'gemini-url-needs-proxy-p
              "iri"         0)
     (gen-rpc "gemini-request"
              'gemini-request
              "iri"                           0
              "use-cache"                     1
              "titan-data"                    2
              "ignore-certificate-expiration" 3)
     (gen-rpc "gemini-stream-info"
              'gemini-stream-info
              "iri" 0)
     (gen-rpc "gemini-stream-parsed-line"
              'gemini-stream-parsed-line
              "iri" 0
              "line-number" 1)
     (gen-rpc "gemini-stream-parsed-line-slice"
              'gemini-stream-parsed-line-slice
              "iri" 0
              "line-number-start" 1
              "line-number-end"   2)
     (gen-rpc "gemini-all-stream-info"
              'gemini-all-stream-info)
     (gen-rpc "gemini-stream-status"
              'gemini-stream-status
              "iri" 0)
     (gen-rpc "gemini-remove-stream"
              'gemini-remove-stream
              "iri" 0)
     (gen-rpc "gemini-stream-completed-p"
              'gemini-stream-completed-p
              "iri" 0)
     (gen-rpc "gemini-current-url"          'gemini-current-url)
     (gen-rpc "gemini-pop-url-from-history" 'gemini-pop-url-from-history)
     (gen-rpc "gemini-push-url-to-history"
              'gemini-push-url-to-history
              "iri" 0)
     (gen-rpc "gemini-save-url-db-history"
              'gemini-save-url-db-history
              "iri" 0)
     (gen-rpc "gemini-certificates"         'gemini-certificates)
     (gen-rpc "gemini-delete-tofu-certificate"
              'gemini-delete-tofu-certificate
              "host" 0)
     (gen-rpc "gemini-delete-client-certificate"
              'gemini-delete-client-certificate
              "url" 0)
     (gen-rpc "gemini-import-certificate"
              'gemini-import-certificate
              "uri"       0
              "cert-file" 1
              "key-file"  2)
     (gen-rpc "gemini-save-certificate-key-password"
              'gemini-save-certificate-key-password
              "certificate-path" 0
              "password"         1)
     (gen-rpc "gemini-url-using-certificate-p"
              'gemini-url-using-certificate-p
              "url" 0)
     (gen-rpc "gemini-url-certificate-keypath"
              'gemini-url-certificate-keypath
              "url" 0)
     (gen-rpc "gemini-clear-certificate-password-db"
              'gemini-clear-certificate-password-db)
     (gen-rpc "gemini-table-of-contents"
              'gemini-table-of-contents
              "iri" 0
              "width" 1)
     (gen-rpc "gemini-parse-local-file"
              'gemini-parse-local-file
              "path" 0)
     (gen-rpc "gemini-parse-string"
              'gemini-parse-string
              "string" 0)
     (gen-rpc "gemini-history-rows"
              'gemini-history-rows
              "days-from" 0
              "days-to" 1)
     (gen-rpc "make-error-page"
              'make-error-page
              "iri" 0
              "code" 1
              "description" 2
              "meta" 3)
     (gen-rpc "gemini-slurp-local-file"
              'gemini-slurp-local-file
              "path" 0)
     (gen-rpc "tour-shuffle" 'tour-shuffle)
     (gen-rpc "tour-add-link"
              'tour-add-link
              "link-value" 0
              "link-label" 1)
     (gen-rpc "tour-pop-link" 'tour-pop-link)
     (gen-rpc "tour-delete-link"
              'tour-delete-link
              "iri" 0)
     (gen-rpc "clear-tour"   'clear-tour)
     (gen-rpc "tour-all-links" 'tour-all-links)
     (gen-rpc "gemini-generate-bookmark-page" 'gemini-generate-bookmark-page)
     (gen-rpc "gemini-generate-raw-bookmark-page" 'gemini-generate-raw-bookmark-page)
     (gen-rpc "gemini-bookmark-add" 'gemini-bookmark-add
              "iri"     0
              "section" 1
              "description" 2)
     (gen-rpc "gemini-bookmark-delete" 'gemini-bookmark-delete "iri" 0)
     (gen-rpc "gemini-bookmark-table"  'gemini-bookmark-table)
     (gen-rpc "gemini-bookmarked-p"    'gemini-bookmarked-p "iri" 0)
     (gen-rpc "gemini-gemlog-subscribe" 'gemini-gemlog-subscribe "iri" 0)
     (gen-rpc "gemini-gemlog-subscribed-p" 'gemini-gemlog-subscribed-p "iri" 0)
     (gen-rpc "gemini-gemlog-all-subscription" 'gemini-gemlog-all-subscription)
     (gen-rpc "gemini-gemlog-refresh-all-subscriptions"
              'gemini-gemlog-refresh-all-subscriptions)
     (gen-rpc "gemini-gemlog-unsubscribe" 'gemini-gemlog-unsubscribe "iri" 0)
     (gen-rpc "gemini-gemlog-entries-page"     'gemini-gemlog-entries-page
              "iri" 0
              "title" 1
              "subtitle" 2)
     (gen-rpc "gemini-gemlog-entries"'gemini-gemlog-entries
              "iri" 0)
     (gen-rpc "gemini-purge-gemlog-entries" 'gemini-purge-gemlog-entries)
     (gen-rpc "gemini-mark-gemlog-post-read" 'gemini-mark-gemlog-post-read
              "iri" 0)
     (gen-rpc "titan-save-token" 'titan-save-token
              "url" 0
              "token" 1)
     (gen-rpc "titan-saved-token" 'titan-saved-token
              "url" 0)
     (gen-rpc "iri-to-parent-path" 'iri-to-parent-path "iri" 0)
     (gen-rpc "quit-program" 'quit-program)
     (gen-rpc "purge-history" 'purge-history)
     (gen-rpc "gempub-search" 'gempub-search "query" 0)
     (gen-rpc "gempub-synchronize-library" 'gempub-synchronize-library)
     (gen-rpc "gempub-file-p"              'gempub-file-p "path" 0)
     (gen-rpc "gempub-index-path"          'gempub-index-path "path" 0)
     (gen-rpc "gempub-file-id->path"       'gempub-file-id->path "id" 0)
     (gen-rpc "gempub-file-id->row"        'gempub-file-id->row "id" 0)
     ,@body))
