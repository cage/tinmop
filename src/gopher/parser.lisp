;; tinmop: a multiprotocol client
;; Copyright © cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :gopher-parser)

(defmacro def-line-type-constant (name value documentation)
  `(a:define-constant ,(format-fn-symbol t "+line-type-~a+" name) ,value
     :test #'string=
     :documentation ,documentation))

(defmacro gen-line-constants (name-values-doc-list)
  `(progn
     ,@(loop for data in name-values-doc-list
             collect
             `(def-line-type-constant ,(first  data) ,(second data) ,(third data)))))

(gen-line-constants ((file             "0" "identifier for a file")
                     (dir              "1" "identifier for a directory")
                     (cso              "2" "identifier for a CSO phone-book server")
                     (error            "3" "identifier for an error")
                     (mac-hex-file     "4" "identifier for a BinHexed Macintosh file")
                     (dos-archive-file "5" "identifier for a DOS binary archive of some sort")
                     (uuencoded-file   "6" "identifier for a UNIX uuencoded file")
                     (index-search     "7" "identifier for Index-Search server")
                     (telnet-session   "8" "identifier for a text-based telnet session.")
                     (binary-file      "9" "identifier for a binary file")
                     (redundant-server "+" "identifier for a redundant server")
                     (tn3270-session   "T" "identifier for a tn3270 session")
                     (gif-image-file   "g" "identifier for an image in GIF")
                     (image-file       "I" "identifier for an image file")
                     (info             "i" "information line")
                     (uri              "h" "hyperlink")
                     (empty            ""  "empty line")))

(a:define-constant +gopher-scheme+ "gopher" :test #'string=)

(defun %check-line-type (data reference)
  (string= data reference))

(defmacro %gen-check-line-predicate (name reference)
  (a:with-gensyms (data)
  `(defun ,(format-fn-symbol t "%line-type-~a-p" name) (,data)
     (%check-line-type ,data ,reference))))

(%gen-check-line-predicate file             +line-type-file+)

(%gen-check-line-predicate dir              +line-type-dir+)

(%gen-check-line-predicate cso              +line-type-cso+)

(%gen-check-line-predicate error            +line-type-error+)

(%gen-check-line-predicate mac-hex-file     +line-type-mac-hex-file+)

(%gen-check-line-predicate dos-archive-file +line-type-dos-archive-file+)

(%gen-check-line-predicate uuencoded-file   +line-type-uuencoded-file+)

(%gen-check-line-predicate index-search     +line-type-index-search+)

(%gen-check-line-predicate telnet-session   +line-type-telnet-session+)

(%gen-check-line-predicate binary-file      +line-type-binary-file+)

(%gen-check-line-predicate redundant-server +line-type-redundant-server+)

(%gen-check-line-predicate tn3270-session   +line-type-tn3270-session+)

(%gen-check-line-predicate gif-file         +line-type-gif-image-file+)

(%gen-check-line-predicate image-file       +line-type-image-file+)

(%gen-check-line-predicate info             +line-type-info+)

(%gen-check-line-predicate uri              +line-type-uri+)

(%gen-check-line-predicate empty            +line-type-empty+)

(defclass gopher-line ()
  ((line-type-id
    :initarg :line-type-id
    :initform ""
    :accessor line-type-id
    :type     string)
   (username
    :initarg :username
    :initform ""
    :accessor username
    :type     string)
   (selector
    :initarg :selector
    :initform ""
    :accessor selector
    :type     string)
   (host
    :initarg :host
    :initform ""
    :accessor host
    :type     string)
   (port
    :initarg :port
    :initform -1
    :accessor port
    :type     number)))

(defmethod print-object ((object gopher-line) stream)
  (with-accessors ((username username)
                   (selector selector)
                   (host     host)
                   (port     port)) object
    (print-unreadable-object (object stream :type t)
      (format stream
              "username: ~s selector: ~s host: ~s port ~a"
              username selector host port))))

(defmacro gen-selector-class (name)
  `(defclass ,name (gopher-line) ()))

(gen-selector-class line-file)

(gen-selector-class line-dir)

(gen-selector-class line-cso)

(gen-selector-class line-error)

(gen-selector-class line-mac-hex-file)

(gen-selector-class line-dos-archive-file)

(gen-selector-class line-uuencoded-file)

(gen-selector-class line-index-search)

(gen-selector-class line-telnet-session)

(gen-selector-class line-binary-file)

(gen-selector-class line-redundant-server)

(gen-selector-class line-tn3270-session)

(gen-selector-class line-gif-file)

(gen-selector-class line-image-file)

(gen-selector-class line-info)

(gen-selector-class line-uri)

(gen-selector-class line-unknown)

(gen-selector-class line-empty)

(defun check-line-type (data reference)
  (typep data reference))

(defmacro gen-check-line-predicate (name reference)
  (a:with-gensyms (data)
  `(defun ,(format-fn-symbol t "line-type-~a-p" name) (,data)
     (check-line-type ,data ,reference))))

(gen-check-line-predicate file             'line-file)

(gen-check-line-predicate dir              'line-dir)

(gen-check-line-predicate cso              'line-cso)

(gen-check-line-predicate error            'line-error)

(gen-check-line-predicate mac-hex-file     'line-mac-hex-file)

(gen-check-line-predicate dos-archive-file 'line-dos-archive-file)

(gen-check-line-predicate uuencoded-file   'line-uuencoded-file)

(gen-check-line-predicate index-search     'line-index-search)

(gen-check-line-predicate telnet-session   'line-telnet-session)

(gen-check-line-predicate binary-file      'line-binary-file)

(gen-check-line-predicate redundant-server 'line-redundant-server)

(gen-check-line-predicate tn3270-session   'line-tn3270-session)

(gen-check-line-predicate gif-file         'line-gif-image-file)

(gen-check-line-predicate image-file       'line-image-file)

(gen-check-line-predicate info             'line-info)

(gen-check-line-predicate uri              'line-uri)

(gen-check-line-predicate unknown          'line-unknown)

(gen-check-line-predicate empty            'line-empty)

(defrule gopher-line-separator (and #\Return #\Newline)
  (:constant :line-separator))

(defrule gopher-field-separator #\tab
  (:constant :field-separator))

(defrule gopher-null-char #\Nul
  (:constant :field-separator))

(defrule gopher-unascii (not (or gopher-field-separator gopher-line-separator gopher-null-char))
  (:text t))

(defrule gopher-last-line (and #\. gopher-line-separator)
  (:constant :last-line))

(defrule gopher-line-type gopher-unascii
  (:text t))

(defrule gopher-red-type (and #\+ #\.)
  (:constant :red-type))

(defrule gopher-user-name (* gopher-unascii)
  (:text t))

(defrule gopher-selector (* gopher-unascii)
  (:text t))

(defrule gopher-hostname-component (* (not (or gopher-field-separator gopher-line-separator
                                               gopher-null-char
                                        #\.)))
  (:text t))

(defrule gopher-host (and (* (and gopher-hostname-component #\.))
                          gopher-hostname-component)
  (:text t))

(defrule gopher-digit (character-ranges #\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9)
  (:text t))

(defrule gopher-digit-sequence (and gopher-digit (* gopher-digit))
  (:text t))

(defrule gopher-port gopher-digit-sequence
  (:function parse-integer))

(defrule gopher-dir-entity (or (and gopher-line-type gopher-user-name gopher-field-separator
                                    gopher-selector gopher-field-separator
                                    gopher-host gopher-field-separator
                                    gopher-port gopher-line-separator)
                               gopher-line-separator)
  (:function (lambda (line)
               (if (listp line)
                   (list :type      (first line)
                         :user-name (second line)
                         :selector  (fourth line)
                         :host      (sixth  line)
                         :port      (elt    line 7))
                   (list :type      +line-type-empty+
                         :user-name ""
                         :selector  ""
                         :host      ""
                         :port      "")))))

(defrule gopher-menu (and (* gopher-dir-entity) (? gopher-last-line))
  (:function first))

(defun has-line-type-p (data)
  (or (%line-type-file-p data)
      (%line-type-dir-p data)
      (%line-type-cso-p data)
      (%line-type-error-p data)
      (%line-type-mac-hex-file-p data)
      (%line-type-dos-archive-file-p data)
      (%line-type-uuencoded-file-p data)
      (%line-type-index-search-p data)
      (%line-type-telnet-session-p data)
      (%line-type-binary-file-p data)
      (%line-type-redundant-server-p data)
      (%line-type-tn3270-session-p data)
      (%line-type-gif-file-p data)
      (%line-type-image-file-p data)
      (%line-type-info-p data)
      (%line-type-uri-p data)
      (%line-type-empty-p data)))

(defun parse-menu (data)
  (let ((menu (parse 'gopher-menu data)))
    (loop for entry in menu
          collect
          (let* ((line-type (getf entry :type))
                 (instance  (cond
                              ((%line-type-file-p line-type)
                               (make-instance 'line-file))
                              ((%line-type-dir-p line-type)
                               (make-instance 'line-dir))
                              ((%line-type-cso-p line-type)
                               (make-instance 'line-cso))
                              ((%line-type-error-p line-type)
                               (make-instance 'line-error))
                              ((%line-type-mac-hex-file-p line-type)
                               (make-instance 'line-mac-hex-file))
                              ((%line-type-dos-archive-file-p line-type)
                               (make-instance 'line-dos-archive-file))
                              ((%line-type-uuencoded-file-p line-type)
                               (make-instance 'line-uuencoded-file))
                              ((%line-type-index-search-p line-type)
                               (make-instance 'line-index-search))
                              ((%line-type-telnet-session-p line-type)
                               (make-instance 'line-telnet-session))
                              ((%line-type-binary-file-p line-type)
                               (make-instance 'line-binary-file))
                              ((%line-type-redundant-server-p line-type)
                               (make-instance 'line-redundant-server))
                              ((%line-type-tn3270-session-p line-type)
                               (make-instance 'line-tn3270-session))
                              ((%line-type-gif-file-p line-type)
                               (make-instance 'line-gif-file))
                              ((%line-type-image-file-p line-type)
                               (make-instance 'line-image-file))
                              ((%line-type-info-p line-type)
                               (make-instance 'line-info))
                              ((%line-type-uri-p line-type)
                               (make-instance 'line-uri))
                              ((%line-type-empty-p line-type)
                               (make-instance 'line-empty))
                              (t
                               (make-instance 'line-unknown)))))
            (setf (line-type-id instance) (getf entry :type)
                  (username     instance) (getf entry :user-name)
                  (selector     instance) (getf entry :selector)
                  (host         instance) (getf entry :host)
                  (port         instance) (getf entry :port))
            instance))))

(defrule gopher-text-block (+ (not (and #\Newline #\. #\Return #\Newline)))
  (:text t))

(defrule gopher-text-file (and (* gopher-text-block)
                               (? (and #\Newline #\. #\Return #\Newline)))
  (:function caar))

(defun parse-text-file (data)
  (parse 'gopher-text-file data))

(defrule gopher-gopher-url-authority (or (and (+ (not #\:))
                                       #\:
                                       (+ (not #\/))
                                       #\/)
                                  (and (+ (not #\/))
                                       #\/))
  (:function (lambda (a)
               (let* ((host-port a)
                      (host (coerce (first host-port) 'string))
                      (port (if (third host-port)
                                (parse-integer (coerce (third host-port) 'string))
                                70)))
                 (list host port)))))

(defrule gopher-gopher-url (and (+ (not #\:))
                                "://"
                                gopher-gopher-url-authority
                                (? (character-ranges (#\u0000 #\uffff)))
                                (* (character-ranges (#\u0000 #\uffff))))
  (:function (lambda (a)
               (let* ((host-port (third a))
                      (host (coerce (first host-port) 'string))
                      (port (if (third host-port)
                                (parse-integer (coerce (third host-port) 'string))
                                70))
                      (type-path (fourth a))
                      (type      (if type-path
                                     type-path
                                     +line-type-dir+))
                      (path      (coerce (fifth a) 'string)))
                 (list host
                       port
                       (percent-decode path)
                       (string type))))))

(defun parse-iri (iri)
  (let* ((parsed   (parse 'gopher-gopher-url iri))
         (host     (first parsed))
         (port     (second parsed))
         (selector (third  parsed))
         (type     (fourth parsed)))
    (values host port type selector)))
