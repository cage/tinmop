;; tinmop: a multiprotocol client
;; Copyright © cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :iri-parser)

(define-constant +segment-separator+ "/" :test #'string=)

(defrule iri-alpha (character-ranges (#\a #\z) (#\A #\Z))
  (:text t))

(defrule iri-digit (character-ranges (#\0 #\9))
  (:text t))

(defrule iri-scheme-delim #\:
  (:constant :scheme-delim))

(defrule iri-iquery-delim #\?
  (:constant :iquery-delim))

(defrule iri-ifragment-delim #\#
  (:constant :ifragment-delim))

(defrule iri-port-delim #\:
  (:constant :port-delim))

(defrule iri-credential-delim #\@
  (:constant :credential-delim))

(defrule iri-iauthority-start "//"
  (:constant :iauthority-start))

(defrule iri-sub-delims  (or #\!  #\$  #\&  #\'  #\(  #\) #\*  #\+  #\,  #\;  #\=)
  (:text t))

(defrule iri-gen-delims (or ":" "?"  "#"  "["  "]"  "@" "")
  (:text t))

(defrule iri-iunreserved-chars (or iri-alpha iri-digit  #\-  #\.  #\_  #\~ iri-ucschar)
  (:text t))

(defrule iri-iprivate (or (character-ranges (#\UE000 #\UF8FF))
                          (character-ranges (#\UF0000 #\UFFFFD))
                          (character-ranges (#\U100000 #\U10FFFD)))
  (:text t))


(defrule iri-ucschar (or (character-ranges (#\UA0    #\UD7FF))
                     (character-ranges (#\UF900  #\UFDCF))
                     (character-ranges (#\UFDF0  #\UFFEF))
                     (character-ranges (#\U10000 #\U1FFFD))
                     (character-ranges (#\U20000 #\U2FFFD))
                     (character-ranges (#\U30000 #\U3FFFD))
                     (character-ranges (#\U40000 #\U4FFFD))
                     (character-ranges (#\U50000 #\U5FFFD))
                     (character-ranges (#\U60000 #\U6FFFD))
                     (character-ranges (#\U70000 #\U7FFFD))
                     (character-ranges (#\U80000 #\U8FFFD))
                     (character-ranges (#\U90000 #\U9FFFD))
                     (character-ranges (#\UA0000 #\UAFFFD))
                     (character-ranges (#\UB0000 #\UBFFFD))
                     (character-ranges (#\UC0000 #\UCFFFD))
                     (character-ranges (#\UD0000 #\UDFFFD))
                     (character-ranges (#\UE1000 #\UEFFFD)))
  (:text t))

(defrule iri-reserved-chars (or iri-gen-delims iri-sub-delims)
  (:text t))

(defrule iri-scheme (and iri-alpha (* (or iri-alpha iri-digit  "+"  "-"  "." )))
  (:text t))

(defrule iri-ihier-part (or (and iri-iauthority-start iri-iauthority iri-ipath-abempty)
                        iri-ipath-absolute ; text
                        iri-ipath-rootless ;text
                        iri-ipath-empty)) ;text

(defrule iri-user-credentials (and iri-iuserinfo iri-credential-delim)
  (:function first))

(defrule iri-port-block (and iri-port-delim iri-port)
  (:function second)
  (:function parse-integer))

(defrule iri-iauthority (and (? iri-user-credentials)
                        iri-ihost
                        (? iri-port-block)))

(defrule iri-ireg-name (* (or iri-iunreserved-chars iri-pct-encoded iri-sub-delims ))
  (:text t))

(defrule iri-ihost (or iri-ipv4-address iri-ip-literal iri-ireg-name)
  (:text t))

(defrule iri-port (+ iri-digit)
  (:text t))

(defrule iri-iuserinfo (* (or iri-iunreserved-chars iri-pct-encoded iri-sub-delims  ":" ))
  (:text t))

(defrule iri-pct-encoded (and "%" iri-hexdig iri-hexdig)
  (:text t))

(defrule iri-hexdig (or (character-ranges (#\a #\f))
                        (character-ranges (#\A #\F))
                        iri-digit)
  (:text t))

(defrule iri-ipv4-address (and iri-dec-octet "."
                               iri-dec-octet "."
                               iri-dec-octet "."
                               iri-dec-octet)
  (:text t))

(defrule iri-ip-literal  (and "["
                          (+ (not (or "[" "]")))
                          "]")
  (:function (lambda (a) (text (second a)))))

(defrule iri-ipchar (or iri-iunreserved-chars iri-pct-encoded iri-sub-delims  ":"  "@")
  (:text t))

(defrule iri-isegment (* iri-ipchar)
  (:text t))

(defrule iri-isegment-non-zero (+ iri-ipchar)
  (:text t))

(defrule iri-isegment-nz-nc (+ (or iri-iunreserved-chars
                                   iri-pct-encoded
                                   iri-sub-delims  "@" ))
  (:text t))

(defrule iri-ipath-abempty (* (and "/" iri-isegment))
  (:text t))

(defrule iri-ipath  (or iri-ipath-abempty
                        iri-ipath-absolute
                        iri-ipath-noscheme
                        iri-ipath-rootless
                        iri-ipath-empty)
  (:text t))

(defrule iri-ipath-absolute (and "/" (? (and iri-isegment-non-zero (* (and "/"
                                                                           iri-isegment )))))
  (:text t))

(defrule iri-ipath-rootless (and iri-isegment-non-zero (* (and "/" iri-isegment )))
  (:text t))

(defrule iri-ipath-noscheme (and iri-isegment-nz-nc (* (and  "/" iri-isegment )))
  (:text t))

(defrule iri-ipath-empty ""
  (:constant nil))

(defun octet-p (maybe-octet)
  (ignore-errors
   (let ((number (parse-integer (text-utils:strcat* maybe-octet))))
     (when (<= 0 number 255)
       number))))

(defrule iri-dec-octet (octet-p (+ iri-digit))
  (:text t))

(defun extract-fields-from-absolute-iri (parsed)
  (let* ((scheme           (first parsed))
         (ihier-part       (third parsed))
         (authority        (when (consp ihier-part)
                             (second ihier-part)))
         (user-credentials (first authority))
         (host             (second authority))
         (port             (third authority))
         (ipath            (if (consp ihier-part)
                               (third ihier-part)
                               ihier-part))
         (iquery           (fourth parsed))
         (ifragment        (fifth  parsed)))
    (list scheme
          user-credentials
          host
          port
          ipath
          iquery
          ifragment)))

(defrule iri-iri (and iri-scheme ":"
                      iri-ihier-part
                      (? iri-iquery)
                      (? iri-ifragment))
  (:function extract-fields-from-absolute-iri))

(defrule iri-irelative-part (or (and iri-iauthority-start
                                     iri-iauthority
                                     iri-ipath-abempty)
                                iri-ipath-absolute
                                iri-ipath-noscheme
                                iri-ipath-empty))

(defun extract-fields-from-relative-iri-w-authority (parsed)
  ;; ((:IAUTHORITY-START (NIL "bar.baz" NIL) "/foo.gmi") "a=b" "afrag")
  (let ((authority (second (first parsed)))
        (path      (third  (first parsed))))
    (list nil                ; scheme
          (first  authority) ; user-credentials
          (second authority) ; host
          (third  authority) ; port
          path
          (second parsed)    ; iquery
          (third  parsed)))) ; fragment

(defun extract-fields-from-relative-iri-w/o-authority (parsed)
  (list nil                ; scheme
        nil                ; user-credentials
        nil                ; host
        nil                ; port
        (first parsed)     ; path
        (second parsed)    ; iquery
        (third  parsed)))  ; fragment

(defun extract-fields-from-relative-iri (parsed)
  (if (consp (first parsed))
      (extract-fields-from-relative-iri-w-authority   parsed)
      (extract-fields-from-relative-iri-w/o-authority parsed)))

(defrule iri-irelative-ref (and iri-irelative-part (? iri-iquery) (? iri-ifragment))
  (:function extract-fields-from-relative-iri))

(defrule iri-iquery (and iri-iquery-delim (* (or iri-ipchar
                                                 iri-iprivate
                                                 "/"
                                                 "?")))
  (:function second)
  (:text t))

(defrule iri-ifragment (and iri-ifragment-delim (* (or iri-ipchar "/" "?")))
  (:function second)
  (:text t))

(defrule iri-iri-reference (or iri-iri iri-irelative-ref))

(defclass iri ()
  ((scheme
    :initform nil
    :initarg  :scheme
    :accessor scheme)
   (user-info
    :initform nil
    :initarg  :user-info
    :accessor user-info)
   (host
    :initform nil
    :initarg  :host
    :writer   (setf host))
   (port
    :initform nil
    :initarg  :port
    :accessor port)
   (path
    :initform nil
    :initarg  :path
    :accessor path)
   (query
    :initform nil
    :initarg  :query
    :accessor query)
   (fragment
    :initform nil
    :initarg  :fragment
    :accessor fragment)))

(defgeneric host (object))

(defmethod host ((object iri))
  (let ((host (slot-value object 'host)))
    (if (text-utils:string-starts-with-p "[" host)
        (subseq host 1 (1- (length host)))
        host)))

(defmethod print-object ((object iri) stream)
  (print-unreadable-object (object stream)
    (format stream
            "~s ~s ~s ~s ~s ~s ~s"
            (iri:scheme    object)
            (iri:user-info object)
            (iri:host      object)
            (iri:port      object)
            (iri:path      object)
            (iri:query     object)
            (iri:fragment  object))))

(defun make-iri (&optional scheme user-info host port path query fragment)
  (make-instance 'iri
                 :scheme    scheme
                 :user-info user-info
                 :host      host
                 :port      port
                 :path      path
                 :query     query
                 :fragment  fragment))

(defun iri-parse (iri &key (null-on-error nil))
  (handler-case
      (let* ((parsed (parse 'iri-iri-reference iri :junk-allowed nil))
             (res    (mapcar (lambda (a) (cond
                                           ((typep a 'string)
                                            (if (text-utils:string-empty-p a)
                                                nil
                                                a))
                                           (t a)))
                             (list (first   parsed)       ; scheme
                                   (second  parsed)       ; user-credentials
                                   (third   parsed)       ; host
                                   (fourth  parsed)       ; port
                                   (fifth   parsed)       ; path
                                   (sixth   parsed)       ; query
                                   (seventh parsed)))))   ; fragment
        (values (apply #'make-iri res)
                res))
    (esrap:esrap-parse-error (e)
      (if null-on-error
          nil
          (error e)))))

(defun copy-iri (from)
  (let ((scheme    (iri:scheme    from))
        (user-info (iri:user-info from))
        (host      (slot-value from 'iri:host))
        (port      (iri:port      from))
        (path      (iri:path      from))
        (query     (iri:query     from))
        (fragment  (iri:fragment  from)))
    (make-iri scheme
              user-info
              host
              port
              path
              query
              fragment)))

(defgeneric remove-fragment (iri))

(defmethod remove-fragment ((object iri))
  (let ((copied (copy-iri object)))
    (setf (iri:fragment copied) nil)
    copied))

(defmethod normalize-path ((object iri))
  (let ((clean-path (fs:normalize-path (iri:path object)))
        (copy       (copy-iri  object)))
    (when clean-path
      (setf (iri:path copy) clean-path))
    copy))

(defun render-iri (iri &optional (stream *standard-output*))
  (flet ((render ()
           (with-output-to-string (string-stream)
             (let ((scheme    (iri:scheme    iri))
                   (user-info (iri:user-info iri))
                   (host      (slot-value    iri 'iri:host))
                   (port      (iri:port      iri))
                   (path      (iri:path      iri))
                   (query     (iri:query     iri))
                   (fragment  (iri:fragment  iri)))
               (when scheme
                 (format string-stream "~a:" scheme))
               (when host
                 (write-string "//" string-stream))
               (when user-info
                 (format string-stream "~a@" user-info))
               (when host
                 (format string-stream "~a" host))
               (when port
                 (format string-stream ":~a" port))
               (when path
                 (format string-stream "~a" path))
               (when query
                 (format string-stream "?~a" query))
               (when fragment
                 (format string-stream "#~a" fragment))))))
    (if stream
        (write-string (render) stream)
        (render))))

(defmethod to-s ((object iri) &key &allow-other-keys)
  (with-output-to-string (stream)
    (render-iri object stream)))

(defun network-path-url-p (url)
  (when-let ((iri (iri:iri-parse url :null-on-error t)))
    (and (null (iri:scheme iri))
         (iri:host iri))))

(defun relative-url-p (url)
  (when-let ((iri (iri:iri-parse url :null-on-error t)))
    (and (null (iri:scheme iri))
         (null (iri:host   iri)))))

(defun absolute-url-p (url)
  (when-let ((parsed (iri:iri-parse url :null-on-error t)))
    (and (not (network-path-url-p url))
         (not (relative-url-p url))
         (iri:host parsed)
         (iri:scheme parsed))))

(defun absolute-url-scheme-p (url expected-scheme)
  (when-let ((parsed-iri (iri:iri-parse url :null-on-error t)))
    (and (absolute-url-p url)
         (string= (iri:scheme parsed-iri) expected-scheme))))

(defun ipv4-address-p (string)
  (ignore-errors
   (let ((bytes (mapcar #'parse-integer
                        (cl-ppcre:split "\\."
                                        string))))
     (and (= (length bytes)
             4)
          (every (lambda (a) (<= 0 a 255)) bytes)))))

(defun ipv6-address-p (string)
  (cl-ppcre:scan ":" string))

(defun iri-to-parent-path (iri)
  (let* ((parsed-iri  (iri:iri-parse iri))
         (parent-path (fs:parent-dir-path (iri:path parsed-iri)))
         (new-iri     (to-s (make-instance 'iri:iri
                                           :scheme    (iri:scheme    parsed-iri)
                                           :host      (iri:host      parsed-iri)
                                           :user-info (iri:user-info parsed-iri)
                                           :port      (iri:port      parsed-iri)
                                           :path      parent-path))))
    new-iri))

(defgeneric iri= (a b))

(defmethod iri= ((a iri) (b iri))
  (let ((scheme-a    (iri:scheme    a))
        (user-info-a (iri:user-info a))
        (host-a      (iri:host      a))
        (port-a      (iri:port      a))
        (path-a      (iri:path      a))
        (query-a     (iri:query     a))
        (fragment-a  (iri:fragment  a))
        (scheme-b    (iri:scheme    b))
        (user-info-b (iri:user-info b))
        (host-b      (iri:host      b))
        (port-b      (iri:port      b))
        (path-b      (iri:path      b))
        (query-b     (iri:query     b))
        (fragment-b  (iri:fragment  b)))
    (and (string= scheme-a    scheme-b)
         (string= user-info-a user-info-b)
         (string= host-a      host-b)
         (string= port-a      port-b)
         (string= path-a      path-b)
         (string= query-a     query-b)
         (string= fragment-a  fragment-b))))

(defmethod iri= ((a iri) (b string))
  (when-let ((parsed (iri-parse b :null-on-error t)))
    (iri= a parsed)))

(defmethod iri= ((a string) (b iri))
  (when-let ((parsed (iri-parse a :null-on-error t)))
    (iri= b parsed)))

(defmethod iri= ((a string) (b string))
  (let ((parsed-a (iri-parse a :null-on-error t))
        (parsed-b (iri-parse b :null-on-error t)))
    (and parsed-a
         parsed-b
         (iri= parsed-a parsed-b))))
