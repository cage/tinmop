.TH "tinmop" "1" 
.SH "Name"
.PP
tinmop - a client for gemini, gopher, mastodon and pleroma social network and kamid (9p protocol over TLS)
.SH "Synopsis"
.PP
tinmop [-h|--help] [-v|--version] [-f|--folder FOLDER-NAME]
       [-t|--timeline TIMELINE-NAME] [-u|--update-timeline] [-R|--reset-timeline-pagination]
       [-c|--check-follows-requests] [-e|--execute-script SCRIPT-FILE] [-m|--notify-mentions]
       [-o|--open-net-address NET-ADDRESS] [-G|--gemini-client-only] [-U|--gemini-gui-client-only]
       [-A|--fediverse-authorization-code] [-S|--gemini-gui-server-only] [-M|--load-module MODULE-FILE]
       [-F|--fediverse-account FEDIVERSE-ACCOUNT] [-X|--lisp-dependencies-uris] [--start-dummy-server]
       [-Q|--search-fediverse SEARCH-FEDIVERSE-QUERY] [--bash-complete]
.SH "Description"
.PP
This document assumes basic knowledge of how fediverse works. More information about this topic can be found on the following websites:

.IP \(em 4
\fIhttps://en.wikipedia.org/wiki/Fediverse\fP ;
.IP \(em 4
\fIhttps://pleroma.social/\fP ;
.IP \(em 4
\fIhttps://docs.joinmastodon.org/\fP .

.PP
Tinmop proposes an extensible terminal interface to connect with Mastodon or Pleroma social network, the gemini (for connecting to gemspace an optional graphical interface is also available) and gopher protocol.

.IP \(em 4
\fIgemini://geminiprotocol.net/docs/\fP
.IP \(em 4
\fIhttps://geminiprotocol.net/docs/\fP

.PP
Finally tinmop can  connect to network file system  using 9p protocol over TLS (using client certificates as authentication method).

.PP
For more information about this feature, please visit: \fIhttps://kamid.omarpolo.com/\fP.
.SH "Command line options"
.PP
Without options the program will start a terminal interface and will try to connect to your instance (see \fIfuzzy:Configuration\fP), the most useful command line options follows

.TP
\fB\fC\-\-bash\-complete\fP                              \fP
Complete command line switches (for bash completion only).
.TP
\fB\fC\-\-start\-dummy\-server\fP                         \fP
Start a dummy sever, just useful for debugging.
.TP
\fB\fC\-A, \-\-fediverse\-authorization\-code\fP           \fP
Asks for server authorization code, before connecting to the fediverse instance.
.TP
\fB\fC\-F, \-\-fediverse\-account FEDIVERSE\-ACCOUNT\fP    \fP
Specify a fediverse user account (format: user-name@server-name).
.TP
\fB\fC\-G, \-\-gemini\-client\-only\fP                     \fP
Start as gemini client only.
.TP
\fB\fC\-M, \-\-load\-module MODULE\-FILE\fP                \fP
Load a module.
.TP
\fB\fC\-Q, \-\-search\-fediverse SEARCH\-FEDIVERSE\-QUERY\fP\fP
Search fediverse.
.TP
\fB\fC\-R, \-\-reset\-timeline\-pagination\fP              \fP
Reset the timeline pagination.
.TP
\fB\fC\-S, \-\-gemini\-gui\-server\-only\fP                 \fP
Start as gemini gui server only.
.TP
\fB\fC\-U, \-\-gemini\-gui\-client\-only\fP                 \fP
Start as gemini gui client only.
.TP
\fB\fC\-X, \-\-lisp\-dependencies\-uris\fP                 \fP
Download lisp libraries (useful for packaging only).
.TP
\fB\fC\-c, \-\-check\-follows\-requests\fP                 \fP
Check follows requests.
.TP
\fB\fC\-e, \-\-execute\-script SCRIPT\-FILE\fP             \fP
Execute script.
.TP
\fB\fC\-f, \-\-folder FOLDER\-NAME\fP                     \fP
Starting folder
.TP
\fB\fC\-h, \-\-help\fP                                   \fP
Print help and exit.
.TP
\fB\fC\-m, \-\-notify\-mentions\fP                        \fP
Notify messages that mentions the user.
.TP
\fB\fC\-o, \-\-open\-net\-address NET\-ADDRESS\fP           \fP
Open net address.
.TP
\fB\fC\-t, \-\-timeline TIMELINE\-NAME\fP                 \fP
Starting timeline.
.TP
\fB\fC\-u, \-\-update\-timeline\fP                        \fP
Update timeline.
.TP
\fB\fC\-v, \-\-version\fP                                \fP
Print program information and exit.
.SH "Usage"
.PP
Users of Tinmop supposed to interact with the social network using a  terminal interface (TUI, but starting  with version 0.9.9.1414 a GUI for gemini is available See \fIfuzzy:GUI\fP), the terminal screen layout is sketched below:

.RS
.nf
\fC+---------------+---------------------------+
|               |                           |
| tags window   |      thread windows       |
|               |                           |
|               |  modeline                 |
+---------------+---------------------------+
|               |                           |
| conversations |      main window          |
|    window     |                           |
|               |                           |
|               |                           |
+---------------+---------------------------+
|              command window               |
+-------------------------------------------+

\fP
.fi
.RE

.PP
The screen is subdivided in five window:

.TP
\fBtag window\fP
shows the tag users subscribed for and available messages for each tag;

.TP
\fBthreads window\fP
for a given timeline and folder (see \fIfuzzy:Folders\fP) show the discussions saved in user's local database;

.TP
\fBconversations window\fP
show the \fIprivate\fP conversations the user is having with others;

.TP
\fBmain window\fP
show the body of the message selected in the tag window or gemini page

.TP
\fBcommand window\fP
the window where the user instruct the software to perform commands

.PP
Using the program in gemini exclusive mode (option \fB"-G"\fP) the program layout is simplified:

.RS
.nf
\fC
+------------------------------------------+
|                                          |
|               main window                |
|                                          |
|                                          |
|                                          |
+------------------------------------------+
|              command window              |
+------------------------------------------+

\fP
.fi
.RE

.PP
The main way to interact with the program is using the keyboard. By default you can move focus to each window (except command window that can not get focus explicitly) using \fB'M-arrow key'\fP (meta is \fBALT\fP on many keyboards). There is a contextual help that appears when the user input data that provide hints about commands and a quick help window that can be shown by hitting \fC?\fP (if this keybinding has not been customized).
.SS "Command window keys"
.PP
The command window has a few hardcoded command keys to interact with it:

.IP \(em 4
the \fBleft\fP and \fBright\fP arrow keys move the cursor;
.IP \(em 4
the key \fBhome\fP and \fBend\fP move the cursor to the start and end of the input respectively;
.IP \(em 4
\fBcanc\fP and \fBbackspace\fP delete the next and previous character respectively;
.IP \(em 4
the \fBnew line\fP (often called \fBenter\fP key) send the input to the program;
.IP \(em 4
\fBC-k\fP (that is: "press 'control' and while pressed press 'k') \fIkills\fP (deletes) the text from the cursor position to the end of the input previously typed;
.IP \(em 4
\fBC-g\fP will cancel the prompt and quit the command;
.IP \(em 4
press \fBM-left\fP and \fBM-right\fP (\fBleft alt\fP and \fBleft\fP or \fBright\fP arrow together) to browse pages of the suggestion window; the suggestion window is a window that holds a previously inputted data that are compatible with the string the user is typing into the command window;
.IP \(em 4
if suggestions are gemini URI press \fBTAB\fP to input the current selected suggestion;
.IP \(em 4
if suggestion window is \fBnot\fP rendered, pressing \fBup\fP and \fBdown\fP arrow keys will cycle through input history, if there is not a suggestion window rendered pressing \fBup\fP and \fBdown\fP will scroll on suggestions.
.SS "Folders"
.PP
A folder is an object to groups messages for each timeline an arbitrary number of folders can be created, when the last message of a folder is deleted the folder is deleted as well.
.SS "GUI"
.PP
Calling this program with option \fB"-U"\fP will starts with a gemini client with a GUI mode. The Usage should be intuitive for persons that are used to use programs with a graphical interface and mouse interaction, here are the default keybindings:

.TP
\fBquit                      \fP
Control-q
.TP
\fBsearch                    \fP
Alt-f
.TP
\fBstream                    \fP
Alt-s
.TP
\fBcertificates              \fP
Alt-c
.TP
\fBtour manage               \fP
Alt-t
.TP
\fBtour next                 \fP
Control-t
.TP
\fBtour shuffle              \fP
Alt-S
.TP
\fBgemlog                    \fP
Alt-g
.TP
\fBabout                     \fP
Alt-a
.TP
\fBtype-address              \fP
Alt-d
.TP
\fBview-source               \fP
Alt-u
.TP
\fBgo back in address history\fP
Control-BackSpace
.TP
\fBup                        \fP
U
.TP
\fBbookmark toggle           \fP
Control-d
.TP
\fBbookmark show             \fP
Alt-b
.TP
\fBbookmark export           \fP
Alt-e
.TP
\fBbookmark import           \fP
Alt-i
.TP
\fBsearch gempub library     \fP
Alt-l
.TP
\fBgemtext scaling increase  \fP
Control-plus
.TP
\fBgemtext scaling decrease  \fP
Control-minus
.TP
\fBgemtext scaling reset     \fP
Control-0
.TP
\fBgemtext refresh           \fP
Control-r
.SH "Configuration"
.PP
The configuration of tinmop is based on text files but they are available two different kinds with different syntax and scope.

.IP \(em 4
a key-value text files used to configure the access credential to server and visual theme of the program (simple configuration);

.IP \(em 4
common lisp source code. Used to write module (AKA plugin) and to configure keybindings to interact with the software.

.PP
The distribution of this software comes with a bunch of pre-backed configuration files
(see \fIfuzzy:Files\fP) but user is expected to write a simple file with their credential to log into the server, if you want to use tinmop as a fediverse client.

.PP
The system wide configuration files are automatically included in user defined files, the configuration directive in custom files override the system wide ones.
.SS "Simple configuration"
.PP
This is a simple file with each entry in a single line that look like this:

.RS
.nf
\fC
# a line starting with a '#' is a comment

# a file can be included in another with this directive:
use "shared.conf"

# The server instance name
server = server address

# your username
username = username

\fP
.fi
.RE

.PP
Not incidentally the information in the example above are the absolute minimum the user has to provide before starts the program and connect to pleroma/mastodon (to use tinmop as a gemini browser only an empty file will suffice): the name you chose when you made the account on the server and the address of the server.

.PP
As you can see a line starting with a \fB#\fP is considered comment and skipped by the program

.PP
The file with this credential are confidential and must be put into user's home directory under the path
\fC$HOME/.local/share/tinmop/main.conf\fP. Probably the directory \fCtinmop\fP does not exists on user system; if it does not exists will be created by the software the first time it will run.

.PP
If the program was installed correctly two other files with simple semantics are located in your system wide configuration directory (usually \fC/etc/tinmop/\fP), please check these files for more information, as they are extensively commented.

.PP
Is worth mentioning again that, without an user configuration file, the program can be used as gemini client (see the "-G" command line switch on top of this manual).
.SS "Lisp program"
.PP
These files contains Common lisp (see \fIhttps://common-lisp.net/\fP) source code. They are used both as a way to configure the program and to write module for tinmop itself.

.PP
These files are the only way to configure program's keybindings: sequence of pressing button to fire command commands (do not worry it is not too difficult!).

.PP
These files must be a valid Common Lisp program to allow the program to even starts. Again this is actual source code that is loaded end executed by the main program; be careful, do not copy and paste code from untrusted sources as this could results in a \fBsevere\fP security damage.

.PP
Again in the configuration directory there is a (commented) file named \fCinit.lisp\fP that user can use as their starting point to write their files. A custom init file, or other module files, must be located into the directory \fC$HOME/.local/share/tinmop/\fP.

.PP
However there is no need to write their own init file if user is happy with the provided one by the package maintainers.
.SH "First time start"
.PP
After the configuration, the program can be started but we are not ready to join the fediverse yet because tinmop need to be \fItrusted\fP by the server. Just follows the instruction on screen to register the application with your instance. This procedure should be followed once: when the program starts for the first time (but please note that there must be a file with valid credentials available).

.PP
There is no additional steps to follow to connect to gemspace, instead.
.SH "Gempub support"
.PP
Tinmop maintains a gempub library scanning a directory on your file system (library directory); the library directory path can be set using the configuration directive:

.RS
.nf
\fC
gempub.directory.library = /absolute/path/to/your/gempub/library

\fP
.fi
.RE

.PP
the default value is \fC$XDG_DATA_HOME\fP (usually something like
\fC$HOME/.local/share/tinmop/\fP).

.PP
Using \fB'M-g g l'\fP the library can be inspected using a simple query language (similar to SQL) that search in the metadata of the gempub files:


.RS
.nf
\fC
*where* criteria

\fP
.fi
.RE

.PP
for example:

.RS
.nf
\fC
where author like "calvino" and published < "1980"

where author like "c%al" or published = "1980"

\fP
.fi
.RE

.PP
Valid search keys are:

.IP \(em 4
title;
.IP \(em 4
author;
.IP \(em 4
language;
.IP \(em 4
description;
.IP \(em 4
publish-date;
.IP \(em 4
revision-date;
.IP \(em 4
published;
.IP \(em 4
copyright.

.PP
You can use \fC< > = != <= >= like\fP operators for comparison and the two logical operator \fCand\fP and \fCor\fP; in the case of the \fClike\fP operator the character \fC%\fP act like a wildcard and means: \fI'any sequence of character'\fP. A \fC%\fP is implicitly added at both the ends of the string, that is \fClike "foo"\fP will match any string that contains "foo".

.PP
Note that the right hand side of the operator must be wrapped in quotes.

.PP
After the search is performed a window with the results is shown, selecting an item of this window will open the gempub and will add all its table of contents on the tour mode, so that the book could be browsed.

.PP
For more information on tour mode see below or use \fB'C-h A <enter> tour mode'\fP.
.SH "Tour mode"
.PP
Tinmop maintains a queue of links that can be then visited by the user, this queue is called \fBtour\fP.

.PP
There are two ways to add a link to the tour:

.IP \(em 4
when a link window is open and focused pressing \fBt\fP will start a prompt for link indices to be saved in the tour; the prompt expect a simple, comma or space separated, list of indices or index range, index range is a couple of index separated by a dash, example below:

.RS
.nf
\fC1 2 5 8-12
\fP
.fi
.RE

.PP
The string above will save the link index number 1, 2, 3, 5, 8, 9, 10, 11, 12 to the tour.

.PP
The other way to add links to the tour is using the command \fB'gemlog-add-unread-posts-tour'\fP (default keychord: \fB'M-g s t a'\fP) that will add all the unread posts to the tour.

.PP
There are a few more useful command to manipulate the tour:

.TP
\fBnext-tour-link\fP
(\fB'M-t t'\fP or just 't' if a gemini window is focused) follows the next link in the tour;

.TP
\fBshow-tour-links\fP
(\fB'M-t s'\fP or just \fB'T'\fP if a gemini window is focused) shows the contents of the tour in a link window

.TP
\fBclean-all-tour\fP
(\fB'M-t c'\fP)

.TP
\fBshuffle-tour\fP
(\fB'M-t S'\fP) shuffle the contents of the tour
.SH "How to get more help"
.PP
For help with pleroma visit the pleroma website:

.PP
\fIhttps://pleroma.social/\fP

.PP
The mastodon website can be found at:

.PP
\fIhttps://joinmastodon.org/\fP

.PP
For information about gemini:

.PP
$ tinmop -o gemini://geminiprotocol.net/

.PP
The program has an inline help (default binding for help is "?"), a manpage (default binding to view the manpage is "C-h m") and inline help that can be searched (default: "C-h A").

.PP
Moreover you can have some useful hint at the program web page:

.PP
\fIhttps://www.autistici.org/interzona/tinmop/\fP
.SH "Default keybindings for TUI"
.SS "Global keymap"
.TP
\fB!        \fP
gemini-search
.TP
\fB>        \fP
open-net-address
.TP
\fB@        \fP
switch-fediverse-account
.TP
\fB?        \fP
print-quick-help
.TP
\fBC-d      \fP
clear-cache
.TP
\fBC-I      \fP
pass-focus-next
.TP
\fBC-a      \fP
show-about-window
.TP
\fBA        \fP
show-announcements
.TP
\fBC-h A    \fP
apropos-help-global
.TP
\fBC-h a    \fP
apropos-help
.TP
\fBC-h h    \fP
print-quick-help
.TP
\fBC-h m    \fP
open-manual
.TP
\fBC-q      \fP
quit
.TP
\fBM-c      \fP
open-chats-list-window
.TP
\fBM-down   \fP
pass-focus-on-bottom
.TP
\fBM-e      \fP
eval-command
.TP
\fBM-g c i  \fP
import-gemini-certificate
.TP
\fBM-g c s  \fP
gemini-open-certificates-window
.TP
\fBM-g g b s\fP
display-bookmark
.TP
\fBM-g g l  \fP
open-gempub-library
.TP
\fBM-g s o  \fP
gemini-open-gemlog-window
.TP
\fBM-g s r  \fP
gemlog-refresh-all
.TP
\fBM-g s t a\fP
gemlog-add-unread-posts-tour
.TP
\fBM-l      \fP
load-script-file
.TP
\fBM-left   \fP
pass-focus-on-left
.TP
\fBM-right  \fP
pass-focus-on-right
.TP
\fBM-s l    \fP
message-window-lock-scrolling
.TP
\fBM-s u    \fP
message-window-unlock-scrolling
.TP
\fBM-t S    \fP
shuffle-tour
.TP
\fBM-t a    \fP
open-gemini-links-and-ask-tour
.TP
\fBM-t c    \fP
clean-all-tour
.TP
\fBM-t s    \fP
show-tour-links
.TP
\fBM-t t    \fP
next-tour-link
.TP
\fBM-up     \fP
pass-focus-on-top
.TP
\fBq        \fP
confirm-and-clean-close-program
.SS "Follow request window"
.TP
\fBC-J (key \fIenter\fP) \fP
process-follow-requests
.TP
\fBd   \fP
follow-request-delete
.TP
\fBdown\fP
follow-request-go-down
.TP
\fBj   \fP
follow-request-go-down
.TP
\fBk   \fP
follow-request-go-up
.TP
\fBq   \fP
cancel-follow-requests
.TP
\fBup  \fP
follow-request-go-up
.SS "Send post window"
.TP
\fBC-J (key \fIenter\fP) \fP
send-message
.TP
\fBd   \fP
attach-delete
.TP
\fBdown\fP
attach-go-down
.TP
\fBe   \fP
edit-message-body
.TP
\fBj   \fP
attach-go-down
.TP
\fBk   \fP
attach-go-up
.TP
\fBm   \fP
change-mentions
.TP
\fBq   \fP
cancel-send-message
.TP
\fBC-g \fP
cancel-send-message
.TP
\fBs   \fP
change-subject
.TP
\fBup  \fP
attach-go-up
.TP
\fBv   \fP
change-visibility
.SS "Thread window"
.TP
\fB[        \fP
thread-go-to-parent-post
.TP
\fB^        \fP
thread-open-parent-post
.TP
\fB/ q      \fP
search-fediverse
.TP
\fB/ Q      \fP
search-fediverse-local (see: \fIfuzzy:Fediverse local search\fP)
.TP
\fB/ b      \fP
thread-search-next-message-body
.TP
\fB/ m      \fP
thread-search-next-message-meta
.TP
\fBC-J (key \fIenter\fP)\fP
thread-open-selected-message
.TP
\fBC-X m b  \fP
boost-selected-status
.TP
\fBC-X m f  \fP
favourite-selected-status
.TP
\fBC-X m r b\fP
unboost-selected-status
.TP
\fBC-X m r f\fP
unfavourite-selected-status
.TP
\fBC-X m s  \fP
subscribe-to-hash
.TP
\fBC-X m t  \fP
move-message-tree
.TP
\fBC-X m u  \fP
unsubscribe-to-hash
.TP
\fBC-c c    \fP
change-conversation-name
.TP
\fBC-c o    \fP
open-conversation
.TP
\fBC-c u    \fP
update-conversations
.TP
\fBC-f c    \fP
change-folder
.TP
\fBC-t R    \fP
reset-timeline-pagination
.TP
\fBC-t U    \fP
update-current-timeline-backwards
.TP
\fBC-t c    \fP
change-timeline
.TP
\fBC-t h r  \fP
refresh-tags
.TP
\fBC-t u    \fP
update-current-timeline
.TP
\fBC-u c k g\fP
crypto-generate-key
.TP
\fBC-u c k i\fP
crypto-import-key
.TP
\fBC-u c k s\fP
crypto-export-key
.TP
\fBC-u f    \fP
follow-user
.TP
\fBC-u i    \fP
ignore-user
.TP
\fBC-u r f  \fP
start-follow-request-processing
.TP
\fBC-u r r  \fP
report-status
.TP
\fBC-u u    \fP
unfollow-user
.TP
\fBC-u v a  \fP
view-user-avatar
.TP
\fBC-u x    \fP
unignore-user
.TP
\fBD        \fP
delete-post-using-regex
.TP
\fBI        \fP
print-post-id
.TP
\fBM        \fP
print-mentions
.TP
\fBM-u      \fP
delete-and-move-previous
.TP
\fBN d      \fP
delete-notifications
.TP
\fBT d      \fP
thread-delete-subtree
.TP
\fBe        \fP
edit-posted-message
.TP
\fBP        \fP
poll-vote
.TP
\fBp        \fP
show-parent-post
.TP
\fBU        \fP
thread-mark-prevent-delete-selected-message
.TP
\fBX        \fP
refresh-thread-totally
.TP
\fB/ /      \fP
repeat-search
.TP
\fB\\ b     \fP
thread-search-previous-message-body
.TP
\fB\\ m     \fP
thread-search-previous-message-meta
.TP
\fBc        \fP
compose-message
.TP
\fBd        \fP
delete-and-move-next
.TP
\fBdc (key \fIdelete\fP)\fP
thread-mark-delete-selected-message
.TP
\fBdown     \fP
thread-go-down
.TP
\fBend      \fP
thread-goto-last-message
.TP
\fBg        \fP
thread-goto-message
.TP
\fBhome     \fP
thread-goto-first-message
.TP
\fBj        \fP
thread-go-down
.TP
\fBk        \fP
thread-go-up
.TP
\fBl        \fP
open-message-link
.TP
\fBleft     \fP
open-previous
.TP
\fBn        \fP
thread-search-next-unread-message
.TP
\fBu        \fP
thread-search-next-undeleted-message
.TP
\fBr        \fP
reply-message
.TP
\fBright    \fP
open-next
.TP
\fBs d      \fP
status-tree->text
.TP
\fBup       \fP
thread-go-up
.TP
\fBv        \fP
open-message-attach
.TP
\fBx        \fP
refresh-thread
.TP
\fB|        \fP
send-message-to-pipe
.SS "Fediverse local search"
.PP
The database containing posts fetched from the server can be searched using a simple query language (loosely similar to SQL):

.RS
.nf
\fC
*where* criteria [*into* folder-name]

\fP
.fi
.RE

.PP
examples:

.RS
.nf
\fC
where rendered-text like "calvino" and username like "cage" into writer-review

where rendered-text like "calvino" and (username like "cage" or rebblogged =1)


\fP
.fi
.RE

.PP
Valid search keys are:

.IP \(em 4
account-id;
.IP \(em 4
account;
.IP \(em 4
application;
.IP \(em 4
content;
This columns holds the original content of the post in HTML format;
.IP \(em 4
favourited;
.IP \(em 4
favourites-count;
.IP \(em 4
folder;
.IP \(em 4
language;
.IP \(em 4
muted;
Boolean value: 0 or 1;
.IP \(em 4
reblogged;
.IP \(em 4
reblogs-count;
.IP \(em 4
redp;
.IP \(em 4
rendered-text;
This columns holds the content of the post as displayed on the program window;
.IP \(em 4
replies-count;
.IP \(em 4
sensitive;
Boolean value: 0 or 1;
.IP \(em 4
spoiler-text;
.IP \(em 4
status-id;
.IP \(em 4
tags;
.IP \(em 4
timeline;
.IP \(em 4
uri;
the address of the post in JSON format
.IP \(em 4
url;
The internet address of the HTML representation of the post;
.IP \(em 4
username;
.IP \(em 4
visibility.

.PP
You can use \fC< > = != <= >= like\fP operators for comparison and the two logical operator \fCand\fP and \fCor\fP; in the case of the \fClike\fP operator the character \fC%\fP act like a wildcard and means: \fI'any sequence of character'\fP. A \fC%\fP is implicitly added at both the ends of the string, that is \fClike "foo"\fP will became "%foo%" and will matches any string that contains "foo".

.PP
Note that the right hand side of the operator must be wrapped in quotes.

.PP
The results are saved in the folder specified after the clause \fIinto\fP (if omitted a default folder name is used), note that the folder name can not contains spaces.
.SS "Posts window"
.TP
\fB/    \fP
message-search-regex
.TP
\fBC-J (key \fIenter\fP)  \fP
message-scroll-down
.TP
\fBN    \fP
repeat-search
.TP
\fBd    \fP
delete-shown-post
.TP
\fBdown \fP
message-scroll-down
.TP
\fBend  \fP
message-scroll-end
.TP
\fBhome \fP
message-scroll-begin
.TP
\fBj    \fP
message-scroll-down
.TP
\fBk    \fP
message-scroll-up
.TP
\fBl    \fP
message-extract-links
.TP
\fBleft \fP
message-scroll-left
.TP
\fBnpage\fP
message-scroll-next-page
.TP
\fBppage\fP
message-scroll-previous-page
.TP
\fBright\fP
message-scroll-right
.TP
\fBup   \fP
message-scroll-up
.TP
\fB|    \fP
send-to-pipe
.SS "Gemini viewer window"
.TP
\fB/    \fP
message-search-regex
.TP
\fBB    \fP
address-go-back-in-path
.TP
\fBC-J (key \fIenter\fP)  \fP
message-scroll-down
.TP
\fBC-[  \fP
go-to-previous-link
.TP
\fBC-]  \fP
go-to-next-link
.TP
\fBC-b a\fP
bookmark-gemini-page
.TP
\fBC-b d\fP
delete-gemini-bookmark
.TP
\fBC-b s\fP
display-bookmark
.TP
\fBI M  \fP
gemini-images-montage
.TP
\fBN    \fP
repeat-search
.TP
\fBO    \fP
open-gemini-toc
.TP
\fBR    \fP
address-go-root-path
.TP
\fBT    \fP
show-tour-links
.TP
\fBU    \fP
gemini-view-source
.TP
\fB[    \fP
open-previous-link
.TP
\fB]    \fP
open-next-visible-link
.TP
\fBb    \fP
net-address-history-back
.TP
\fBc    \fP
gemini-open-certificates-window
.TP
\fBd    \fP
gemini-open-streams-window
.TP
\fBdown \fP
message-scroll-down
.TP
\fBend  \fP
message-scroll-end
.TP
\fBhome \fP
message-scroll-begin
.TP
\fBj    \fP
message-scroll-down
.TP
\fBk    \fP
message-scroll-up
.TP
\fBl    \fP
open-message-link
.TP
\fBleft \fP
message-scroll-left
.TP
\fBnpage\fP
message-scroll-next-page
.TP
\fBp    \fP
message-toggle-preformatted-block
.TP
\fBppage\fP
message-scroll-previous-page
.TP
\fBr    \fP
gemini-refresh-page
.TP
\fBright\fP
message-scroll-right
.TP
\fBs    \fP
gemini-subscribe-gemlog
.TP
\fBt    \fP
next-tour-link
.TP
\fBup   \fP
message-scroll-up
.TP
\fB|    \fP
send-to-pipe
.SS "Gemini page table of contents window"
.TP
\fB/   \fP
gemini-toc-search
.TP
\fBC-J (key \fIenter\fP) \fP
gemini-toc-scroll-down-page
.TP
\fBN   \fP
repeat-search
.TP
\fBdown\fP
gemini-toc-scroll-down
.TP
\fBend \fP
gemini-toc-scroll-end
.TP
\fBhome\fP
gemini-toc-scroll-begin
.TP
\fBj   \fP
gemini-toc-scroll-down
.TP
\fBk   \fP
gemini-toc-scroll-up
.TP
\fBn   \fP
gemini-toc-scroll-down-page
.TP
\fBp   \fP
gemini-toc-scroll-up-page
.TP
\fBq   \fP
gemini-toc-close
.TP
\fBC-g \fP
gemini-toc-close
.TP
\fBup  \fP
gemini-toc-scroll-up
.SS "Gemini stream window"
.TP
\fBC-J (key \fIenter\fP) \fP
gemini-streams-window-open-stream
.TP
\fBa   \fP
gemini-abort-download
.TP
\fBdown\fP
gemini-streams-window-down
.TP
\fBj   \fP
gemini-streams-window-down
.TP
\fBk   \fP
gemini-streams-window-up
.TP
\fBq   \fP
gemini-streams-window-close
.TP
\fBup  \fP
gemini-streams-window-up
.SS "Gemini certificates window"
.TP
\fBC-J (key \fIenter\fP) \fP
gemini-certificate-information
.TP
\fBd   \fP
gemini-delete-certificate
.TP
\fBdown\fP
gemini-certificate-window-go-down
.TP
\fBj   \fP
gemini-certificate-window-go-down
.TP
\fBk   \fP
gemini-certificate-window-go-up
.TP
\fBq   \fP
gemini-close-certificate-window
.TP
\fBC-g \fP
gemini-close-certificate-window
.TP
\fBup  \fP
gemini-certificate-window-go-up
.TP
\fBc   \fP
gemini-change-certificate-password
.TP
\fBD   \fP
clear-cached-client-tls-certificates
.SS "Gemini subscription window"
.TP
\fBC-J (key \fIenter\fP)      \fP
show-gemlog-to-screen
.TP
\fBd        \fP
gemlog-cancel-subscription
.TP
\fBdown     \fP
gemlogs-subscription-go-down
.TP
\fBj\fP
gemlogs-subscription-go-down
.TP
\fBk\fP
gemlogs-subscription-go-up
.TP
\fBl        \fP
open-message-link
.TP
\fBq        \fP
close-gemlog-window
.TP
\fBC-g      \fP
close-gemlog-window
.TP
\fBup       \fP
gemlogs-subscription-go-up
.SS "Gempub library window"
.TP
\fBC-J (key \fIenter\fP) \fP
gempub-open-file
.TP
\fBdown\fP
gempub-library-window-go-down
.TP
\fBj   \fP
gempub-library-window-go-down
.TP
\fBk   \fP
gempub-library-window-go-up
.TP
\fBq   \fP
gempub-library-window-close
.TP
\fBC-g \fP
gempub-library-window-close
.TP
\fBup  \fP
gempub-library-window-go-up
.SS "Post's tag window"
.TP
\fBC-J (key \fIenter\fP) \fP
open-tag-folder
.TP
\fBU   \fP
unsubscribe-to-hash
.TP
\fBdown\fP
tag-go-down
.TP
\fBj   \fP
tag-go-down
.TP
\fBk   \fP
tag-go-up
.TP
\fBr   \fP
refresh-tags
.TP
\fBs   \fP
subscribe-to-hash
.TP
\fBup  \fP
tag-go-up
.SS "Conversations window"
.TP
\fBC-J (key \fIenter\fP)  \fP
goto-conversation
.TP
\fBC-c c\fP
change-conversation-name
.TP
\fBI    \fP
ignore-conversation
.TP
\fBdc (key \fIdelete\fP)\fP
delete-conversation
.TP
\fBdown \fP
conversation-go-down
.TP
\fBj    \fP
conversation-go-down
.TP
\fBk    \fP
conversation-go-up
.TP
\fBup   \fP
conversation-go-up
.SS "Attachments window"
.TP
\fBC-J (key \fIenter\fP) \fP
open-message-attach-perform-opening
.TP
\fBO   \fP
open-attachment-ignoring-cache
.TP
\fBC-c \fP
copy-from-attach-window-to-clipboard
.TP
\fBa   \fP
open-all-message-attachments
.TP
\fBdown\fP
open-message-attach-go-down
.TP
\fBj   \fP
open-message-attach-go-down
.TP
\fBk   \fP
open-message-attach-go-up
.TP
\fBq   \fP
close-open-attach-window
.TP
\fBC-g \fP
close-open-attach-window
.TP
\fBup  \fP
open-message-attach-go-up
.SS "Links window"
.TP
\fBC-J \fP
open-message-link-perform-opening
.TP
\fBC-s \fP
open-message-link-perform-fediverse-searching
.TP
\fB/   \fP
search-link-window
.TP
\fBC-c \fP
copy-from-message-link-to-clipboard
.TP
\fBN   \fP
repeat-search
.TP
\fBT   \fP
save-selected-message-in-tour
.TP
\fB]   \fP
gemini-jump-to-link
.TP
\fBdown\fP
open-message-link-go-down
.TP
\fBe   \fP
open-message-link-open-enqueue
.TP
\fBend \fP
open-message-link-window-scroll-end
.TP
\fBhome\fP
open-message-link-window-scroll-begin
.TP
\fBj   \fP
open-message-link-go-down
.TP
\fBk   \fP
open-message-link-go-up
.TP
\fBq   \fP
close-open-message-link-window
.TP
\fBC-g \fP
close-open-message-link-window
.TP
\fBt   \fP
tour-mode-link
.TP
\fBup  \fP
open-message-link-go-up
.SS "Chats list window"
.TP
\fBC-J (key \fIenter\fP) \fP
show-chat-to-screen
.TP
\fBR   \fP
refresh-chats
.TP
\fBc   \fP
chat-create-new
.TP
\fBdown\fP
chat-list-go-down
.TP
\fBj   \fP
chat-list-go-down
.TP
\fBk   \fP
chat-list-go-up
.TP
\fBl   \fP
change-chat-label
.TP
\fBq   \fP
close-chats-list-window
.TP
\fBC-g \fP
close-chats-list-window
.TP
\fBr   \fP
refresh-chat-messages
.TP
\fBup  \fP
chat-list-go-up
.SS "Chat window"
.TP
\fB/        \fP
message-search-regex
.TP
\fBM-c      \fP
write-to-chat
.TP
\fBa        \fP
open-chat-link-window
.TP
\fBdown     \fP
message-scroll-down
.TP
\fBend      \fP
message-scroll-end
.TP
\fBhome     \fP
message-scroll-begin
.TP
\fBj        \fP
message-scroll-down
.TP
\fBk        \fP
message-scroll-up
.TP
\fBnpage    \fP
message-scroll-next-page
.TP
\fBppage    \fP
message-scroll-previous-page
.TP
\fBup       \fP
message-scroll-up
.SS "File explorer"
.TP
\fB/   \fP
file-explorer-search
.TP
\fBC-J (key \fIenter\fP) \fP
file-explorer-open-node
.TP
\fBD   \fP
file-explorer-delete-tree
.TP
\fBM d \fP
file-explorer-download-mirror
.TP
\fBM u \fP
file-explorer-upload-mirror
.TP
\fBM-m \fP
file-explorer-mark-by-regexp
.TP
\fBN   \fP
repeat-search
.TP
\fBX   \fP
file-explorer-delete-marked
.TP
\fBa   \fP
file-explorer-create-path
.TP
\fBc   \fP
file-explorer-close-path
.TP
\fBd   \fP
file-explorer-download-path
.TP
\fBdown\fP
file-explorer-go-down
.TP
\fBe   \fP
file-explorer-edit-file
.TP
\fBend \fP
file-explorer-scroll-end
.TP
\fBhome\fP
file-explorer-scroll-begin
.TP
\fBi   \fP
file-explorer-node-details
.TP
\fBj   \fP
file-explorer-go-down
.TP
\fBk   \fP
file-explorer-go-up
.TP
\fBm   \fP
file-explorer-mark-entry
.TP
\fBq   \fP
file-explorer-close-window
.TP
\fBC-g   \fP
file-explorer-close-window
.TP
\fBr   \fP
file-explorer-rename-path
.TP
\fBu   \fP
file-explorer-upload-path
.TP
\fBup  \fP
file-explorer-go-up
.TP
\fBx   \fP
file-explorer-expand-path
.SS "Gopher window"
.TP
\fBC-g\fP
close-gopher-window
.TP
\fBC-q\fP
close-gopher-window
.TP
\fBq  \fP
close-gopher-window
.TP
\fBC-J (key \fIenter\fP)  \fP
gopher-window:open-menu-link
.TP
\fBC-b a\fP
bookmark-gopher-page
.TP
\fBC-b d\fP
delete-gemini-bookmark
.TP
\fBC-b s\fP
display-bookmark
.TP
\fBb    \fP
net-address-history-back
.TP
\fBdown \fP
gopher-window:go-to-next-link
.TP
\fBj    \fP
gopher-window:go-to-next-link
.TP
\fBk    \fP
gopher-window:go-to-previous-link
.TP
\fBup   \fP
gopher-window:go-to-previous-link
.SH "BUGS"
.PP
There are many, totally unknown, hiding in the code; this is scary! 😱 Please help the programmer to nail them using the https://codeberg.org/cage/tinmop/issues/ \fBat\fP \fIissue tracker\fP.
.SH "Contributing"
.PP
There is always need for help, you can join the developer, sending  patches or https://translate.codeberg.org/engage/tinmop/ \fBat\fP \fItranslating the UI\fP to your favourite language.

.PP
Just point your browser to the https://codeberg.org/cage/tinmop/ \fBat\fP \fIcode repository\fP.

.PP
See also the file CONTRIBUTE.org
.SS "Debug mode"
.PP
If you uncomment the line:

.RS
.nf
\fC;;(push :debug-mode *features*)
\fP
.fi
.RE

.PP
The program will be compiled in \fCdebug\-mode\fP this means that a lot of diagnostic output will be appended to a file named \fCtinmop.log\fP in the directory \fC$HOME/.local/share/tinmop/\fP.
.SH "Files"
.IP \(em 4
\fC$HOME/.local/share/tinmop/db.sqlite3\fP: the program database
.IP \(em 4
\fC$HOME/.local/share/tinmop/client\fP: the program credentials to connect with the instance; \fBkeep private!\fP
.IP \(em 4
\fC$HOME/.local/share/tinmop/tinmop.log\fP: this file is created only for debugging and should not be enabled in binary package distribution (see \fIfuzzy:Contributing\fP).
.IP \(em 4
\fC/etc/tinmop/default\-theme.conf\fP: default visual style
.IP \(em 4
\fC/etc/tinmop/shared.conf\fP: some default configuration not related to themes
.IP \(em 4
\fC/etc/tinmop/shared\-gui.conf\fP: default configuration for GUI
.IP \(em 4
\fC/etc/tinmop/init.lisp\fP: system wide configuration
.IP \(em 4
\fC$HOME/.config/tinmop/init.lisp\fP: user configuration
.IP \(em 4
\fC$HOME/.config/tinmop/main.conf\fP: user configuration (simple format)
.IP \(em 4
\fC$HOME/.config/tinmop/gui.conf\fP: user configuration for GUI (simple format)
.SH "Privacy"
.PP
The author of this software collects no user data information with this software.

.PP
But this software is a client to connect and interact to one or more remote computer. So potentially it could share a lot of information with other actors but just after the user allowed it to do so.

.PP
It is the user responsibility to checks the privacy conditions of the instance this software connect to.

.PP
By default, pressing "!" will contact the remote service located at: "gemini://houston.coder.town/search".

.PP
Moreover launching \fCquick_quicklisp.sh\fP will contact \fIhttps://www.quicklisp.org/\fP, check the https://beta.quicklisp.org/quicklisp.lisp \fBat\fP \fIquicklisp sources\fP for details.
.SH "Acknowledgment"
.PP
My deep thanks to the folks that provided us with wonderful SBCL and Common lisp libraries.

.PP
In particular i want to thanks the authors of the libraries Croatoan and Tooter for their help when I started to develop this program.

.PP
There are more people i borrowed code and data from, they are mentioned in the file LICENSES.org

.PP
This program was born also with the help of CCCP: "Collettivo Computer Club Palermo".

.PP
Also thanks to "barbar" for testing of the installation scripts.
