;; tinmop module for rewrite link URLs before opening
;; Copyright © 2020 cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :modules)

(defun fetch-expired-poll-from-notification (notifications)
  (a:when-let ((polls (remove-if-not (lambda (a) (eq (tooter:kind a)
                                                     :poll))
                                     notifications)))
    (loop for poll in polls do
      (let* ((timeline (thread-window:timeline-type *thread-window*))
             (folder        (thread-window:timeline-folder *thread-window*))
             (status        (tooter:status poll))
             (status-id     (tooter:id status))
             (refresh-event (make-instance 'refresh-thread-windows-event
                                           :priority +minimum-event-priority+
                                           :message-status-id status-id)))
        (notify "Fetching expired poll")
        (program-events:with-enqueued-process ()
          (db:update-db (api-client:get-remote-status status-id)
                        :folder folder
                        :timeline timeline
                        :skip-ignored-p nil)
          (db:renumber-timeline-message-index timeline folder :account-id nil))
        (push-event refresh-event)
        (notify "Poll fetched")))))

(hooks:add-hook 'hooks:*after-getting-all-fediverse-notifications*
                #'fetch-expired-poll-from-notification)
