# tinmop: an humble mastodon client
# Copyright (C) 2020  cage

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.
# If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

ACLOCAL_AMFLAGS      = -I m4

bin_SCRIPTS          = tinmop

ASDF_FILE            = $(PACKAGE).asd

CONF_PATH_FILE       = src/config.lisp

CLEANFILES           = $(bin_SCRIPTS) $(CONF_PATH_FILE)

CONF_PATH_FILE_IN    = src/config.lisp.in

DISTCLEANFILES       = $(CONF_PATH_FILE_IN) $(ASDF_FILE)

BUILT_SOURCES        = $(CONF_PATH_FILE)

EXTRA_DIST           = config.rpath m4/ChangeLog  tinmop.asd README.org src \
LICENSES.org COPYING etc/shared.conf etc/default-theme.conf etc/shared-gui.conf \
etc/init.lisp compare_version.awk

SUBDIRS              = po

dist_doc_DATA        = README.org LICENSES.org CONTRIBUTING.org \
doc/tinmop.org doc/send-toot.lisp NEWS.org ChangeLog AUTHORS

confdir              = $(sysconfdir)/$(PACKAGE)

if ENABLE_COMPLETION
completiondir = $(COMPLETION_DIR)
dist_completion_DATA = data/completion/tinmop
endif

dist_conf_DATA       = etc/init.lisp etc/default-theme.conf etc/shared.conf etc/shared-gui.conf

nobase_dist_pkgdata_DATA    = \
data/cover.gmi \
data/error-pages/51 \
data/error-pages/header-51.png \
data/icons/fmw_arrow-down.png \
data/icons/fmw_arrow-up.png \
data/icons/fmw_back.png \
data/icons/fmw_bullet-go.png \
data/icons/fmw_bus-go.png \
data/icons/fmw_bus.png \
data/icons/fmw_cross.png \
data/icons/fmw_dice.png \
data/icons/fmw_document-accept.png \
data/icons/fmw_document-add.png \
data/icons/fmw_document-delete.png \
data/icons/fmw_document-edit.png \
data/icons/fmw_folder.png \
data/icons/fmw_go.png \
data/icons/fmw_open_tour.png \
data/icons/fmw_page.png \
data/icons/fmw_profile.png \
data/icons/fmw_refresh.png \
data/icons/fmw_rss-add.png \
data/icons/fmw_rss-delete.png \
data/icons/fmw_search.png \
data/icons/fmw_star-blue.png \
data/icons/fmw_star-yellow.png \
data/icons/fmw_text.png \
data/icons/fmw_toc.png \
data/icons/fmw_two-pictures.png \
data/icons/fmw_uparrow.png \
data/images/broken-image.png \
data/modules/delete-by-regex.lisp \
data/modules/expand-abbrev-command-window.lisp \
data/modules/fetch-expired-poll.lisp \
data/modules/next-previous-open.lisp \
data/modules/rewrite-message-urls.lisp \
data/modules/share-gemini-link.lisp \
data/scripts/delete-old-posts.lisp \
data/scripts/export-gemini-subscriptions.lisp \
data/scripts/fetch-post-tree.lisp \
data/scripts/gemget.lisp \
data/scripts/generate-gemlog.lisp \
data/scripts/get-following.lisp \
data/scripts/import-following.lisp \
data/scripts/import-gemini-subscriptions.lisp \
data/scripts/welcome-bot.lisp

dist_man1_MANS       = doc/tinmop.man

$(PACKAGE): $(CONF_PATH_FILE) *.asd src/*
	$(LISP_COMPILER) \
	--dynamic-space-size $(DYNAMIC_MEMORY_SIZE) \
	--eval "(require 'asdf)" \
	--eval "(asdf:initialize-source-registry '(:source-registry :inherit-configuration (:tree \"$$(pwd)/\")))" \
	--eval "(asdf:make '$(PACKAGE) :build-pathname \"../$(PACKAGE)\")" \
	--eval "(uiop:quit)"
	mv src/$(PACKAGE) $(PACKAGE)

$(PACKAGE)-vendored: $(CONF_PATH_FILE) *.asd src/*
	$(LISP_COMPILER) \
	--dynamic-space-size $(DYNAMIC_MEMORY_SIZE) \
	--eval "(require 'asdf)" \
	--eval "(asdf:initialize-source-registry '(:source-registry :ignore-inherited-configuration (:tree \"$$(pwd)/\")))" \
	--eval "(asdf:make '$(PACKAGE) :build-pathname \"../$(PACKAGE)\")" \
	--eval "(uiop:quit)"
	mv src/$(PACKAGE) $(PACKAGE)

$(CONF_PATH_FILE):
	grep "^;" $(CONF_PATH_FILE_IN)                          >  $(CONF_PATH_FILE)
	echo -e "(in-package :config)\n"                        >> $(CONF_PATH_FILE);
	echo "(alexandria:define-constant +sys-data-dir+"       >> $(CONF_PATH_FILE);
	echo -e "\"$(pkgdatadir)\" :test #'string=)\n"          >> $(CONF_PATH_FILE);

	echo "(alexandria:define-constant +sys-conf-dir+"       >> $(CONF_PATH_FILE);
	echo -e  "\"$(confdir)\" :test #'string=)\n"            >> $(CONF_PATH_FILE);

	echo "(alexandria:define-constant +catalog-dir+"        >> $(CONF_PATH_FILE);
	echo -e "\""$(localedir)"\" :test #'string=)\n"         >> $(CONF_PATH_FILE);
	cat  $(CONF_PATH_FILE).in | sed "\/^;;.*$\/d"           >> $(CONF_PATH_FILE);
